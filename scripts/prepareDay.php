<?php

use AdventOfCode\Classes\Preparation;
use AdventOfCode\Exception\InputException;

$rootPath = realpath(dirname(__FILE__) . '/..');
var_dump($rootPath);
require_once $rootPath . '/vendor/autoload.php';

$a = new Preparation($argv, $rootPath);
try {
    $a->prepareFiles();
} catch (InputException $e) {
    $a->printMsg($e->getMessage());
}
