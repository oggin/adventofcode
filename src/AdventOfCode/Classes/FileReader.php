<?php

declare(strict_types=1);

namespace AdventOfCode\Classes;

use PHPUnit\TextUI\TestFileNotFoundException;

class FileReader
{
    private array $lines;
    private string $content;


    public function __construct(string $filename, bool $trimLines = true)
    {
        if (!file_exists($filename)) {
            throw new TestFileNotFoundException($filename);
        }
        $this->content = (string)@file_get_contents($filename);
        if ($trimLines) {
            $this->content = trim($this->content);
        }
        $lines = explode("\n", $this->content);
        $this->lines = $lines;
    }

    /**
     * @return array<string>
     */
    public function getLines(): array
    {
        return $this->lines;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
