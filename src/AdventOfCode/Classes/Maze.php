<?php

namespace AdventOfCode\Classes;

class Maze
{

    private array $visited = [];
    private array $grid = [];
    private int $goX = 0;
    private int $goY = -1;
    private int $curX = 0;
    private int $curY = 0;
    private array $debug = [];
    private array $startPositions = [];
    private string $curWay = 'U';
    private array $mappings = [
        'U' => [0, -1],// => oben
        'R' => [1, 0],// => rechts
        'D' => [0, 1],// => unten
        'L' => [-1, 0],// => links
    ];

    private const  STEP_EXIT = 1;
    private const  STEP_TURN = 2;
    private const  STEP_WALK = 3;
    private const  STEP_LOOP = 4;

    public function __construct(array $lines)
    {
        foreach ($lines as $y => $line) {
            foreach (str_split($line) as $x => $char) {
                $this->grid[$x][$y] = $char;
                if ($char === '^') {
                    $this->curX = $x;
                    $this->curY = $y;
                    $this->startPositions = [$x, $y];
                    $this->markVisited($this->curX, $this->curY);
                }
            }
        }
    }

    public function calcResult2(): ?int
    {
        $cnt = 0;
        foreach ($this->grid as $y => $row) {
            foreach ($row as $x => $char) {
                if ($char !== '.') {
                    continue;
                }
                $this->reset($x, $y);
                $result = self::STEP_WALK;
                while (in_array($result, [self::STEP_TURN, self::STEP_WALK])) {
                    $result = $this->makeStep();
                }
                if ($result === self::STEP_LOOP) {
                    $cnt++;
                    $this->debug[] = [$x, $y];
                }
                $this->setSpaceForPosition($x, $y, '.');
            }

        }
        var_dump($this->debug);
        return $cnt;
    }

    public function calcResult(): int
    {
        $result = null;
        while ($result !== self::STEP_EXIT) {
            $result = $this->makeStep();
        }

        return count($this->visited);
    }

    private function makeStep(): int
    {
        $nextPosition = $this->grid[$this->curX + $this->goX][$this->curY + $this->goY] ?? null;

        return match ($nextPosition) {
            null => self::STEP_EXIT,
            '#' => $this->turn(),
            default => $this->goOneStep()
        };

    }

    private function turn(): int
    {
        $m = ['U', 'R', 'D', 'L'];
        $this->curWay = $m[array_search($this->curWay, $m) + 1] ?? $m[0];
        $this->goX = $this->mappings[$this->curWay][0];
        $this->goY = $this->mappings[$this->curWay][1];
        return self::STEP_TURN;
    }

    private function markVisited($x, $y): int
    {
        $key = $x . '#' . $y;
        $this->visited[$key] ??= [];
        if (in_array($this->curWay, $this->visited[$key])) {
            return self::STEP_LOOP;
        }
        $this->visited[$key][] = $this->curWay;
        return self::STEP_WALK;
    }

    private function goOneStep(): int
    {
        $this->goX = $this->mappings[$this->curWay][0];
        $this->goY = $this->mappings[$this->curWay][1];
        $this->curX += $this->goX;
        $this->curY += $this->goY;
        return $this->markVisited($this->curX, $this->curY) === self::STEP_LOOP ? self::STEP_LOOP : self::STEP_WALK;

    }

    private function setSpaceForPosition(int $x, int $y, string $char): void
    {
        $this->grid[$x][$y] = $char;
    }

    private function reset(int $x, int $y): void
    {
        $this->visited = [];
        $this->setSpaceForPosition($x, $y, '#');
        $this->curX = $this->startPositions[0];
        $this->curY = $this->startPositions[1];
        $this->markVisited($this->curX, $this->curY);
        $this->curWay = 'U';
        $this->goX = 0;
        $this->goY = -1;

    }



}