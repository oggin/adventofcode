<?php
declare(strict_types=1);

namespace AdventOfCode\Classes;

use PHPUnit\Framework\Assert;

class Printer
{
    /**
     * @var array<int,IntegerList>
     */
    private array $numberAtIndexBeforeValues = [];
    private array $commands = [];

    public static function create(array $lines): Printer
    {
        return new self($lines);
    }

    private function __construct(array $lines)
    {
        foreach ($lines as $line) {
            $this->initLine($line);
        }
    }

    public function check(): int
    {
        return array_sum(array_map(function (IntegerList $list) {
            return $this->isCorrect($list) ? $list->getMiddleValue() : 0;
        }, $this->commands));
    }

    public function check2(): int
    {
        return array_sum(array_map(function (IntegerList $list) {
            return $this->fixOrder($list);
        }, $this->getInvalidCommands()));
    }

    /**
     * @return IntegerList[]
     */
    private function getInvalidCommands(): array
    {
        return array_filter($this->commands, function (IntegerList $list) {
            return !$this->isCorrect($list);
        });
    }

    private function fixOrder(IntegerList $list): ?int
    {
        $sorted = new IntegerList();
        foreach ($list->getReverse() as $nextNumber) {
            $sorted->addAtIndex($nextNumber, $this->getIndexToInsert($nextNumber, $sorted));
        }

        return $this->isCorrect($sorted) ? $sorted->getMiddleValue() : null;

    }

    private function getIndexToInsert(int $number, IntegerList $stack): ?int
    {
        $commonElements = $this->numberAtIndexBeforeValues[$number]->getIntersect($stack);
        if ($commonElements->getLength() === 0) {
            return $stack->getLength();
        }
        foreach ($stack as $element) {
            if ($commonElements->contains($element)) {
                return $stack->getFirstIndexOfElement($element);
            }
        }

        Assert::fail('No element found');
    }

    private function setBefore(string $line): void
    {
        $parts = Helper::castElementsToInt(explode("|", $line));
        $number = $parts[0];
        $before = $parts[1];
        $this->numberAtIndexBeforeValues[$number] ??= new IntegerList();
        $this->numberAtIndexBeforeValues[$number]->add($before);
    }

    private function isCorrect(IntegerList $list): bool
    {
        foreach ($list as $idx => $number) {
            if (!($this->checkBefore($number, $list->getSlice(0, $idx)))) {
                return false;
            }
        }

        return true;

    }

    private function checkBefore(int $number, IntegerList $before): bool
    {
        return $this->numberAtIndexBeforeValues[$number]->getIntersect($before)->getLength() === 0;
    }

    /**
     * @param mixed $line
     * @return void
     */
    private function initLine(mixed $line): void
    {
        if (str_contains($line, "|")) {
            $this->setBefore($line);
        } elseif (str_contains($line, ",")) {
            $this->commands[] = new IntegerList(... Helper::castElementsToInt(explode(",", $line)));
        }
    }

}