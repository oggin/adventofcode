<?php

namespace AdventOfCode\Classes;

class Xmas
{
    private int $numOfFound = 0;

    /**
     * @param array<int,array<int,string>> $grid
     */
    public function __construct(private readonly array $grid)
    {
    }

    public static function fromLines(array $lines): self
    {
        return new self(array_map(fn(string $line) => str_split($line), $lines));
    }


    /**
     * iterate over rows and cols executing callback to get value for part1 or part2
     */
    public function loopOverGrid(callable $callback): int
    {
        for ($x = 0; $x < count($this->grid); $x++) {
            for ($y = 0; $y < count($this->grid[0]); $y++) {
                $this->numOfFound += $callback($x, $y);
            }
        }

        return $this->numOfFound;
    }

    public function countMasOccurrences(): int
    {
        $this->loopOverGrid(function ($x, $y) {
            return $this->getValueAtPoint($x, $y) === 'A' && $this->checkCornersForMas($x, $y) ? 1 : 0;
        });
        return $this->numOfFound;
    }

    private function checkCornersForMas($x, $y): bool
    {
        $topLeft = $this->getValueAtPoint($x - 1, $y - 1);
        $topRight = $this->getValueAtPoint($x - 1, $y + 1);
        $bottomLeft = $this->getValueAtPoint($x + 1, $y - 1);
        $bottomRight = $this->getValueAtPoint($x + 1, $y + 1);

        $validCombinations = [
            ['M', 'M', 'S', 'S'],
            ['S', 'S', 'M', 'M'],
            ['S', 'M', 'S', 'M'],
            ['M', 'S', 'M', 'S']
        ];

        return in_array([$topLeft, $topRight, $bottomLeft, $bottomRight], $validCombinations, true);

    }

    public function countXmasOccurrences(string $word): int
    {

        $allDirections = [
            [0, 1],   // Right
            [0, -1],  // Left
            [1, 0],   // Down
            [-1, 0],  // Up
            [1, 1],   // Diagonal down-right
            [-1, -1], // Diagonal up-left
            [1, -1],  // Diagonal down-left
            [-1, 1]   // Diagonal up-right
        ];

        $this->loopOverGrid(function ($x, $y) use ($allDirections,$word) {
            foreach ($allDirections as [$dx, $dy]) {
                $this->numOfFound += $this->checkWord($x, $y, $dx, $dy, $word) ? 1 : 0;
            }
        });

        return $this->numOfFound;
    }

    private function checkWord($x, $y, $dx, $dy, $word): bool
    {
        for ($i = 0; $i < strlen($word); $i++) {
            if ($this->getValueAtPoint($x + $i * $dx, $y + $i * $dy) !== $word[$i]) {
                return false;
            }
        }
        return true;
    }

    private function getValueAtPoint(int $posX, int $posY): ?string
    {
        return $this->grid[$posX][$posY] ?? null;
    }

}