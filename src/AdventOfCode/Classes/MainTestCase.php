<?php

namespace AdventOfCode\Classes;

use AdventOfCode\Exception\AdventOfCodeException;
use PHPUnit\Framework\TestCase;

class MainTestCase extends TestCase
{
    public function assertCalcResult(MainRiddle $riddle, int $expectedResult): void
    {
        try {
            self::assertEquals($expectedResult, $riddle->calcResult(), 'got invalid result for ' . __FUNCTION__);
        } catch (AdventOfCodeException $e) {
            self::fail('AdventOfCodeException was thrown:' . $e->getMessage());
        }
    }

    public function assertCalcResult2(MainRiddle $riddle, int $expectedResult): void
    {
        try {
            self::assertEquals($expectedResult, $riddle->calcResult2(), 'got invalid result for ' . __FUNCTION__);
        } catch (AdventOfCodeException $e) {
            self::fail('AdventOfCodeException was thrown:' . $e->getMessage());
        }
    }

    public function assertCalcResultString(MainRiddle $riddle, string $expectedResult): void
    {
        try {
            self::assertEquals($expectedResult, $riddle->calcResultString(), 'got invalid result for ' . __FUNCTION__);
        } catch (AdventOfCodeException $e) {
            self::fail('AdventOfCodeException was thrown:' . $e->getMessage());
        }
    }

    public function assertCalcResult2String(MainRiddle $riddle, string $expectedResult): void
    {
        try {
            self::assertEquals($expectedResult, $riddle->calcResult2String(), 'got invalid result for ' . __FUNCTION__);
        } catch (AdventOfCodeException $e) {
            self::fail('AdventOfCodeException was thrown:' . $e->getMessage());
        }
    }
}
