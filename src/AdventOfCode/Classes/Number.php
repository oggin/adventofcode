<?php

namespace AdventOfCode\Classes;

class Number
{

    private int $number;

    public function __construct(int $number)
    {
        $this->number = $number;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function compareOrder(Number $number): int
    {
        return $this->number <=> $number->getNumber();
    }

    public function compareDifference(Number $number): int
    {
        return in_array(abs($this->number - $number->getNumber()), [1, 2, 3]);
    }

    public function compare(Number $number, int $order): bool
    {
        return !(($order !== $number->compareOrder($this)) || !$number->compareDifference($this));

    }

}