<?php

namespace AdventOfCode\Classes;

class Math
{
    public static function gcd($a, $b)
    {
        while ($b != 0) {
            $temp = $b;
            $b = $a % $b;
            $a = $temp;
        }
        return $a;
    }

    public static function lcm($a, $b): float|int
    {
        return abs($a * $b) / self::gcd($a, $b);
    }

    public static function calculateLCM($numbers): float|int
    {
        $lcm = 1;

        foreach ($numbers as $number) {
            $lcm = self::lcm($lcm, $number);
        }

        return $lcm;
    }
}