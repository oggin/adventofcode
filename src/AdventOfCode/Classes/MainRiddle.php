<?php

declare(strict_types=1);

namespace AdventOfCode\Classes;

use AdventOfCode\Exception\AdventOfCodeException;
use AdventOfCode\Exception\InputException;

abstract class MainRiddle
{
    /** * @var array<string> */
    protected array $lines;
    protected string $content;
    public static int $day = -1;
    public static int $year = -1;
    public static bool $trimInput = true;
    protected ?string $inputPattern = null;

    /**
     * @throws InputException
     */
    public function __construct()
    {
        $rootPath = realpath(dirname(__FILE__) . '/../../../');
        $fileReader = new FileReader(
            sprintf($rootPath . '/input/_%d/day%d.txt', static::$year, static::$day),
            static::$trimInput
        );
        $this->lines = ($fileReader)->getLines();
        $this->content = $fileReader->getContent();
        if ($this->inputPattern !== null) {
            foreach ($this->lines as $line) {
                $this->validateLine($line);
            }
        }
    }

    /**
     * @throws InputException
     */
    public function validateLine($line): void
    {
        if (!preg_match($this->inputPattern, $line)) {
            throw new InputException(sprintf('input %s does not match pattern %s', $line, $this->inputPattern));
        }
    }

    /**
     * @throws AdventOfCodeException
     */
    abstract public function calcResult(): int;

    /**
     * @throws AdventOfCodeException
     */
    abstract public function calcResult2(): int;

    /**
     * @throws AdventOfCodeException
     */
    public function calcResultString(): string
    {
        throw new AdventOfCodeException(__FUNCTION__ . ' not solved');
    }

    /**
     * @throws AdventOfCodeException
     */
    public function calcResult2String(): string
    {
        throw new AdventOfCodeException(__FUNCTION__ . ' not solved');
    }

    /**
     * @return int[]
     */
    public function getLinesAsInt(): array
    {
        return Helper::castElementsToInt($this->lines);
    }

    /**
     * @return array
     */
    public function getLines(): array
    {
        return $this->lines;
    }
}
