<?php

namespace AdventOfCode\Classes;

use Iterator;

class IntegerList implements Iterator
{
    /** @var int[] */
    private array $list;
    private int $position;

    public function __construct(int ...$numbers)
    {
        $this->list = $numbers;
        $this->position = 0;
    }

    public function add(int ...$numbers): void
    {
        $this->list = array_merge($this->list, $numbers);
    }

    public function addAtIndex(int $number, int $indexToAdd): void
    {
        array_splice($this->list, $indexToAdd, 0, $number);
    }

    public function getList(): array
    {
        return $this->list;
    }

    public function getMiddleValue(): int
    {
        return $this->list[(int)count($this->list) / 2];
    }

    public function getAtIndex(int $index): ?int
    {
        return $this->list[$index] ?? null;
    }

    public function getSlice(int $fromIdx, int $toIdx): IntegerList
    {
        return new IntegerList(... array_slice($this->list, $fromIdx, $toIdx));
    }

    public function getLength(): int
    {
        return count($this->list);
    }

    public function contains(int $number): bool
    {
        return in_array($number, $this->list, true);
    }

    public function getFirstIndexOfElement(int $element): ?int
    {
        return array_search($element, $this->list);
    }

    public function getIntersect(IntegerList $other): IntegerList
    {
        return new IntegerList(... array_intersect($this->getList() ?? [], $other->getList()));
    }

    public function current(): int
    {
        return $this->list[$this->position];
    }

    public function next(): void
    {
        $this->position++;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->list[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function getReverse(): IntegerList
    {
        return new IntegerList(...array_reverse($this->list));
    }
}