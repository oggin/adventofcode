<?php

declare(strict_types=1);

namespace AdventOfCode\Classes;

class CurlResponse
{
    private int $statusCode;
    private string $response;

    /**
     * @param int $statusCode
     * @param string $response
     */
    public function __construct(int $statusCode, string $response)
    {
        $this->statusCode = $statusCode;
        $this->response = $response;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getResponse(): string
    {
        return $this->response;
    }
}
