<?php

declare(strict_types=1);

namespace AdventOfCode\Classes;

use AdventOfCode\Exception\InputException;

class Preparation
{
    private int $day;
    private int $year;
    private array $input;
    private string $rootPath;
    private string $riddleSrc;
    private string $testSrc;

    public function __construct(array $input, string $rootPath)
    {
        $this->input = $input;
        $this->rootPath = $rootPath;
        $this->year = (int)date('Y');
        $this->day = (int)date('d');
    }

    /**
     * @throws InputException
     */
    private function setDayAndYearFromArgv(): void
    {
        if (count($this->input) > 1) {
            $this->day = (int)$this->input[1];
        }
        if (count($this->input) > 2) {
            $this->year = (int)$this->input[2];
        }
        $this->validateDayAndYear();
    }

    /**
     * @throws InputException
     */
    private function assertFileExists(string $riddleSrc): void
    {
        if (!file_exists($riddleSrc)) {
            throw new InputException($riddleSrc . ' not exists');
        }
    }

    /**
     * @throws InputException
     */
    private function copyFileIfNotExists(string $src, string $dest): void
    {
        if (file_exists($dest)) {
            throw new InputException($dest . ' already exists');
        }
        $this->copyFile($src, $dest);
    }

    private function copyFile(string $src, string $dest): void
    {

        $fileSrc = file_get_contents($src);
        $content = str_replace('{X}', (string)$this->day, $fileSrc);
        $content = str_replace('{Y}', (string)$this->year, $content);
        if (file_put_contents($dest, $content) > 0) {
            $this->printMsg(sprintf('copied from %s to %s', $src, $dest));
        } else {
            $this->printMsg(sprintf('could not copy %s to %s', $src, $dest));
        }
    }

    public function printMsg(string $msg): void
    {
        echo $msg . PHP_EOL;
    }

    private function createInputFile(string $inputDest): void
    {
        if (file_exists($inputDest)) {
            $msg = $inputDest . ' already exists ';

            if (file_get_contents($inputDest) > 0) {
                $this->printMsg($msg . 'and is not empty');
                return;
            } else {
                $this->printMsg($msg . 'but is empty');
            }
        } else {
            $this->printMsg('create ' . $inputDest);
            file_put_contents($inputDest, '');
        }
        $data = $this->loadInputFromWeb();

        if ($data === null) {
            return;
        }
        if ($data->getStatusCode() !== 200) {
            $this->printMsg(sprintf('got statuscode %s for getting input data', $data->getStatusCode()));
            return;
        }
        file_put_contents($inputDest, $data->getResponse());
    }

    private function loadInputFromWeb(): ?CurlResponse
    {
        if (!file_exists($this->rootPath . '/scripts/cookie')) {
            return null;
        }
        $this->printMsg('cookie file exists');
        $cookie = file_get_contents($this->rootPath . '/scripts/cookie');
        $rip = new RiddleInputProvider();
        return $rip->getInputForDayAndYear($this->day, $this->year, $cookie);
    }

    /**
     * @throws InputException
     */
    private function createFiles(
        string $riddleDest,
        string $testDest,
        string $inputDest
    ): void {
        $this->assertFileExists($this->riddleSrc);
        $this->assertFileExists($this->testSrc);

        $this->copyFile($this->testSrc, $testDest);
        $this->createInputFile($inputDest);
        $this->copyFileIfNotExists($this->riddleSrc, $riddleDest);
    }

    /**
     * @throws InputException
     */
    public function prepareFiles(): void
    {
        $this->setDayAndYearFromArgv();
        $inputPath = $this->rootPath . '/input/';
        $riddlesPath = $this->rootPath . '/src/AdventOfCode/Riddles/';
        $testPath = $this->rootPath . '/tests/Unit/';

        $this->riddleSrc = $riddlesPath . 'Day.tpl';
        $this->testSrc = $testPath . 'TestDay.tpl';

        $riddleDest = $riddlesPath . sprintf('_%s/Day%d.php', $this->year, $this->day);
        $testDest = $testPath . sprintf('_%s/TestDay%d.php', $this->year, $this->day);
        $inputDest = $inputPath . sprintf('_%s/day%d.txt', $this->year, $this->day);

        $this->createFiles($riddleDest, $testDest, $inputDest);
    }

    /**
     * @throws InputException
     */
    private function validateDayAndYear(): void
    {
        if ($this->day <= 0 || $this->day > 25) {
            throw new InputException("param day has to be between 1 and 25");
        }

        $curYear = (int)date('Y');
        $curMonth = (int)date('m');
        $curDay = (int)date('d');
        if ($this->year < 2015 || $this->year > $curYear) {
            throw new InputException("year has to be greater than or equal 2015 and smaller than current year");
        }

        if ($this->year === $curYear && $curMonth < 12) {
            throw new InputException("input for current year is only available in december");
        }

        if ($this->year === $curYear && $curMonth === 12 && $this->day > $curDay) {
            throw new InputException("input is in future: " . $this->year . ' ' . $this->day);
        }
    }
}
