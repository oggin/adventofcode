<?php

namespace AdventOfCode\Classes;

class NewMaze
{

    private array $guardStartPosition = [];
    private array $grid;

    public function __construct(array $lines)
    {
        $this->grid = array_map(fn(string $line) => str_split($line), $lines);
        $this->setGuardStartPosition();
    }

    /**
     * @return int 0 if the guard is trapped in a loop, otherwise the number of different states visited
     */
    public function isGuardTrappedInLoop(): int
    {
        $guardPosition = $this->guardStartPosition;
        $directions = [
            ["dx" => -1, "dy" => 0], // Up
            ["dx" => 0, "dy" => 1],  // Right
            ["dx" => 1, "dy" => 0],  // Down
            ["dx" => 0, "dy" => -1], // Left
        ];

        $currentDirection = 0; // Start moving up
        $visitedStates = [];
        $differentStates = [];
        $steps = 0;

        while ($steps++ < 10000) {
            $state  = $guardPosition[0] . "," . $guardPosition[1] . "," . $currentDirection;

            // Check if we've been in this state before
            if (isset($visitedStates[$state])) {
                return 0; // Infinite loop detected
            }
            $differentStates[$guardPosition[0] . "," . $guardPosition[1]] = true;
            $visitedStates[$state] = true;

            // Move the guard
            $newX = $guardPosition[0] + $directions[$currentDirection]["dx"];
            $newY = $guardPosition[1] + $directions[$currentDirection]["dy"];


            // Check what is at the new position
            if(($nextPlace = $this->grid[$newX][$newY] ?? null) === null){
                return count($differentStates);
            }
            if ($nextPlace === "#") {
                // Hit an obstacle, rotate 90 degrees clockwise
                $currentDirection = ($currentDirection + 1) % 4;
            } else {
                // Move the guard to the new position
                $guardPosition = [$newX, $newY];
            }
        }
        return $steps >= 10000 ? 0 : count($differentStates);
    }


    public function countGuardInfiniteLoops(): int
    {
        $rows = count($this->grid);
        $cols = count($this->grid[0]);
        $loopCount = 0;

        for ($y = 0; $y < $rows; $y++) {
            for ($x = 0; $x < $cols; $x++) {
                if ($this->grid[$y][$x] === ".") {
                    // Place an obstacle
                    $this->grid[$y][$x] = "#";

                    // Check if the guard gets trapped
                    if ($this->isGuardTrappedInLoop() === 0) {
                        $loopCount++;
                    }

                    // Remove the obstacle
                    $this->grid[$y][$x] = ".";
                }
            }
        }

        return $loopCount;
    }


    private function setGuardStartPosition(): void
    {
        for ($i = 0; $i < count($this->grid); $i++) {
            for ($j = 0; $j < count($this->grid[0]); $j++) {
                if ($this->grid[$i][$j] === "^") {
                    $this->guardStartPosition = [$i, $j];
                    break;
                }
            }
        }
    }
}

