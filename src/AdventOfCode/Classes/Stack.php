<?php

namespace AdventOfCode\Classes;

class Stack
{
    private array $items = [];

    public function add($key, $value): void
    {
        $this->items[$key] = $value;
    }

    public function has($key): bool
    {
        return isset($this->items[$key]);
    }

    public function get($key)
    {
        return $this->items[$key] ?? null;
    }
}
