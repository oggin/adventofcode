<?php

namespace AdventOfCode\Classes;

class RiddleInputProvider
{
    private string $urlTpl = 'https://adventofcode.com/%d/day/%d/input';


    public function getInputForDayAndYear(int $day, int $year, string $cookie): CurlResponse
    {
        $curl = curl_init();

        curl_setopt_array($curl, [CURLOPT_URL => sprintf($this->urlTpl, $year, $day),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => ['Cookie: session=' . $cookie]]);

        $resp = curl_exec($curl);
        $response = new CurlResponse(curl_getinfo($curl, CURLINFO_HTTP_CODE), $resp);
        curl_close($curl);
        return $response;
    }
}
