<?php

namespace AdventOfCode\Exception;

class InputException extends AdventOfCodeException
{
}
