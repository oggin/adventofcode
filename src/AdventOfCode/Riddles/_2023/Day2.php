<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2023\Components\CubeAmount;
use AdventOfCode\Riddles\_2023\Components\CubeGame;

class Day2 extends MainRiddle
{
    public static int $day = 2;
    public static int $year = 2023;

    public function calcResult(): int
    {
        $redCubeAmount = CubeAmount::create('12 red');
        $greenCubeAmount = CubeAmount::create('13 green');
        $blueCubeAmount = CubeAmount::create('14 blue');
        $matchingGamesSum = 0;
        foreach ($this->lines as $line) {
            if (($game = new CubeGame($line))->match($redCubeAmount, $greenCubeAmount, $blueCubeAmount)) {
                $matchingGamesSum += $game->getRound();
            }
        }
        return $matchingGamesSum;
    }

    public function calcResult2(): int
    {
        $powerSum = 0;
        foreach ($this->lines as $line) {
            $powerSum += (new CubeGame($line))->getMaxCubesByColors();
        }
        return (int)$powerSum;

    }

}