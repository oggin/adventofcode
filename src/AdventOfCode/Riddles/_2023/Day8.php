<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2023\Components\CamelPathSolver;
use Unit\_2023\TestDay8;

/**
 * @see TestDay8;
 * @see ../../../../input/_2023/day8.txt
 */
class Day8 extends MainRiddle
{
    public static int $day = 8;
    public static int $year = 2023;

    public function calcResult(): int
    {
        return CamelPathSolver::fromLines($this->lines)->calcResult();
    }

    public function calcResult2(): int
    {
        return CamelPathSolver::fromLines($this->lines)->calcResult2();
    }

}
