<?php

namespace AdventOfCode\Riddles\_2023\Components;

class CubeGame
{
    private int $round;
    /** @var CubeDraw[] */
    private array $draws;

    public function __construct(string $round)
    {
        [$round, $draws] = explode(': ', $round);
        $this->round = (int)str_replace('Game ','',$round);
        foreach (explode('; ', $draws) as $draw) {
            $this->draws[] = CubeDraw::create($draw);
        }
    }

    public function match(CubeAmount ...$cubeAmounts): bool
    {
        foreach ($this->draws as $draw) {
            if(!$draw->match(...$cubeAmounts)) {
                return false;
            }
        }
        return true;
    }

    public function getMaxCubesByColors(): float|int
    {
        $maxCubes = [Cube::BLUE->value => 0, Cube::GREEN->value => 0, Cube::RED->value => 0];
        foreach ($this->draws as $draw) {
            foreach ($draw->getCubeAmounts() as $cubeAmount) {
                $maxCubes[$cubeAmount->getCube()->value] = max($maxCubes[$cubeAmount->getCube()->value], $cubeAmount->getAmount());
            }
        }
        return $maxCubes[Cube::BLUE->value] * $maxCubes[Cube::GREEN->value] * $maxCubes[Cube::RED->value];
    }

    public function getRound(): int
    {
        return $this->round;
    }


}