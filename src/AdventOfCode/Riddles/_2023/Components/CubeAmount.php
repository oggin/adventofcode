<?php

namespace AdventOfCode\Riddles\_2023\Components;

class CubeAmount
{
    private Cube $cube;
    private int $amount;

    public static function create(string $string): self
    {
        $instance = new self();
        [$amount, $cube] = explode(' ', $string);
        $instance->amount = (int)$amount;
        $instance->cube = Cube::from($cube);
        return $instance;
    }

    public function getCube(): Cube
    {
        return $this->cube;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }


}