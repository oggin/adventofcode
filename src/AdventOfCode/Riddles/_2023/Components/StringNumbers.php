<?php

namespace AdventOfCode\Riddles\_2023\Components;

class StringNumbers
{
    private string $string;
    private ?array $first = null;
    private ?array $last = null;

    public static function fromString(string $string): self
    {
        $instance = new self();
        $instance->string = $string;
        return $instance;
    }

    /**
     * advanced solution task two
     * also works like two or three are treated as digits
     * @return int the first and last digit as int
     */
    public function getNumber(): int
    {
        foreach ($this->getMapping() as $value) {
            $this->setFirstAndLast((string)$value);
        }
        return (int)($this->first[1] . $this->last[1]);

    }

    /**
     * simple solution task one
     * @return int the first and last digit as int
     */
    public function getNumberSimple(): int
    {
        $onlyDigits = (string)filter_var($this->string, FILTER_SANITIZE_NUMBER_INT);
        $digitsAsArray = str_split($onlyDigits);
        return (int)(reset($digitsAsArray) . end($digitsAsArray));
    }

    private function getLetter2Number(): array
    {
        return array_combine(['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'], range(1, 9));
    }

    private function getMapping(): array
    {
        return ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 1, 2, 3, 4, 5, 6, 7, 8, 9];
    }


    /**
     * @param string $needle
     * @return array all starting positions of the needle
     */
    public function getAllPositionsForNeedle(string $needle): array
    {
        $positions = [];
        $posLast = 0;
        while (($posLast = strpos($this->string, $needle, $posLast)) !== false) {
            $positions[] = $posLast;
            $posLast = $posLast + strlen($needle);
        }
        return $positions;
    }

    private function lookUp(string $digitString): int
    {
        return $this->getLetter2Number()[$digitString] ?? $digitString;
    }

    /**
     * @param string $digitString the digit as string
     */
    private function setFirstAndLast(string $digitString): void
    {
        if (count($positions = $this->getAllPositionsForNeedle($digitString)) === 0) {
            return;
        }
        $first = $positions[0];
        $last = end($positions);
        if ($this->first === null || $first < $this->first[0]) {
            $this->first = [$first, $this->lookUp($digitString)];
        }
        if ($this->last === null || $last > $this->last[0]) {
            $this->last = [$last, $this->lookUp($digitString)];
        }

    }
}