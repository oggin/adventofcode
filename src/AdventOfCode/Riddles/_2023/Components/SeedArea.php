<?php

namespace AdventOfCode\Riddles\_2023\Components;

class SeedArea
{
    private int $fromStart;
    private int $fromEnd;
    private int $toStart;



    public function __construct(int $toStart, int $fromStart, int $length)
    {
        $this->fromStart = $fromStart;
        $this->fromEnd = $fromStart + $length - 1;
        $this->toStart = $toStart;

    }

    public function isInRange(int $seed): bool
    {
        return $seed >= $this->fromStart && $seed <= $this->fromEnd;
    }

    public function getMappingForSeed(int $seed): int
    {
        return $seed + $this->toStart - $this->fromStart;
    }

}