<?php

namespace AdventOfCode\Riddles\_2023\Components;

use AdventOfCode\Classes\Helper;

class OasisSensor
{

    /**
     * @var int[]
     */
    private array $sequence;
    private array $sequences = [];
    private int $next;

    public function __construct(string $sequence)
    {
        $this->sequence = Helper::castElementsToInt(explode(' ', $sequence));
        $this->initNext();
    }

    public function getNextValue(): int
    {
        return $this->getNextValueByCallBack(function (array $sequence, int $next) {
            return end($sequence) + $next;
        });
    }

    private function getSubSequences(array $sequence): array
    {
        $subSubSequences = [];
        for ($idx = 1; $idx < count($sequence); $idx++) {
            $subSubSequences[] = $sequence[$idx] - $sequence[$idx - 1];
        }
        return $subSubSequences;
    }

    public function getPrevValue(): int
    {
        return $this->getNextValueByCallBack(function (array $sequence, int $next) {
            return $sequence[0] - $next;
        });

    }

    public function initNext(): void
    {
        $subSequence = $this->getSubSequences($this->sequence);
        while (count(array_unique($subSequence)) !== 1) {
            $this->sequences[] = $subSequence;
            $subSequence = $this->getSubSequences($subSequence);
        }

        $this->next = (int)array_unique($subSequence)[0];
    }

    private function getNextValueByCallBack(callable $nextValue): int
    {
        $next = $this->next;
        foreach (array_reverse($this->sequences) as $sequence) {
            $next = $nextValue($sequence , $next);
        }
        return $nextValue($this->sequence , $next);
    }
}