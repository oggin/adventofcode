<?php

namespace AdventOfCode\Riddles\_2023\Components;

class CubeDraw
{
    /** @var CubeAmount[] */
    private array $cubeAmounts;

    //6 red, 10 green
    public static function create(string $string): self
    {
        $instance = new self();
        $cubeAmounts = explode(', ', $string);
        foreach ($cubeAmounts as $cubeAmountData) {
            $cubeAmount = CubeAmount::create($cubeAmountData);
            $instance->cubeAmounts[$cubeAmount->getCube()->value] = $cubeAmount;
        }
        return $instance;
    }

    public function match(CubeAmount ...$cubeAmounts): bool
    {
        foreach ($cubeAmounts as $cubeAmount) {
            if (
                ($cube = $this->cubeAmounts[$cubeAmount->getCube()->value]) &&
                $cube->getAmount() > $cubeAmount->getAmount()
            ) {
                return false;
            }
        }
        return true;
    }

    public function getCubeAmounts(): array
    {
        return $this->cubeAmounts;
    }


}