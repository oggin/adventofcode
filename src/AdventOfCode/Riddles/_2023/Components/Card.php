<?php

namespace AdventOfCode\Riddles\_2023\Components;

use AdventOfCode\Classes\Helper;

class Card
{
    /**@var int[] $numbers * */
    private array $numbers;
    /**@var int[] $numbers * */
    private array $drawnNumbers = [];
    private int $amount = 1;

    public static function fromLine(string $line): Card
    {
        [$cardPart, $numberParts] = explode('|', $line);
        $cardObject = new self();
        $cardObject->drawnNumbers = Helper::castElementsToInt(explode(' ', trim($numberParts)));
        [, $numberString] = explode(':', trim($cardPart));
        $cardObject->numbers = Helper::castElementsToInt(explode(' ', trim($numberString)));
        return $cardObject;
    }

    public function getScore(): int
    {
        return ($size = $this->getMatchCount()) < 2 ? $size : pow(2, $size - 1);
    }

    public function getMatchCount(): int
    {
        return count(array_intersect($this->numbers, $this->drawnNumbers));
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function increaseAmountBy(int $increaseBy = 1): void
    {
        $this->amount += $increaseBy;
    }


}