<?php

namespace AdventOfCode\Riddles\_2023\Components;

class MotorPartSolver
{
    /**
     * @var MotorPartPoint[]
     */
    private array $symbols = [];

    /**
     * @var MotorPart[]
     */
    private array $motorParts = [];

    private bool $startedPart = false;


    public function __construct(array $lines)
    {
        foreach ($lines as $x => $line) {
            $this->linesToMotorParts($line, $x);
        }

    }

    public function calcResult(): int
    {
        $symbols = $this->symbols;
        return array_sum(array_map(static fn(MotorPart $motorPart) => $motorPart->getPartSum(... $symbols), $this->motorParts));
    }

    public function calcResult2(): int
    {
        foreach ($this->getAsterisks() as $asterisk) {
            $asterisk->updateAsteriskAdjacents(... $this->motorParts);
        }

        return array_sum(array_map(static fn(MotorPartPoint $asterisk) => $asterisk->getAdjacentProduct(), $this->getAsterisks()));

    }

    public function linesToMotorParts(string $line, int $x): void
    {

        foreach (str_split($line) as $y => $char) {
             $this->lineToMotorPart(new MotorPartPoint($x, $y, $char));

        }
        $this->startedPart = false;
    }

    public function lineToMotorPart(MotorPartPoint $p): void
    {
        if ($p->isDot()) {
            $this->startedPart = false;
            return;
        }

        if ($p->isSymbol()) {
            $this->symbols[] = $p;
            $this->startedPart = false;
            return;

        }

        if ($this->startedPart === false) {
            $this->motorParts[] = new MotorPart();
            $this->startedPart = true;
        }
        end($this->motorParts)->update($p);


    }


    /**
     * @return MotorPartPoint[]
     */
    private function getAsterisks(): array
    {
        return array_filter($this->symbols, static fn(MotorPartPoint $point) => $point->isAsterisk());

    }

}