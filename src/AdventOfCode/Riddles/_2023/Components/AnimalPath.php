<?php

namespace AdventOfCode\Riddles\_2023\Components;

class AnimalPath
{
    private int $x;
    private int $y;
    private int $maxXAndY;
    private string $symbol;
    private ?AnimalPath $next = null;
    private ?AnimalPath $previous = null;

    public function __construct(int $x, int $y, int $symbol,int $maxXAndY)
    {
        $this->x = $x;
        $this->y = $y;
        $this->maxXAndY = $maxXAndY;
        $this->symbol = $symbol;
    }

    public function setNext(?AnimalPath $next): void
    {
        $this->next = $next;
    }

    public function setPrevious(?AnimalPath $previous): void
    {
        $this->previous = $previous;
    }

    /**
     * | is a vertical pipe connecting north and south.
     * - is a horizontal pipe connecting east and west.
     * L is a 90-degree bend connecting north and east.
     * J is a 90-degree bend connecting north and west.
     * 7 is a 90-degree bend connecting south and west.
     * F is a 90-degree bend connecting south and east.
     * . is ground; there is no pipe in this tile.
     * S is the starting position of the animal; there is a pipe on this tile,
     * but your sketch doesn't show what shape the pipe has.

     *
     */
    public function getPositionNext(AnimalPathMove $move): ?AnimalPathMove
    {
        return match($move->getFrom()) {
            AnimalPathMove::FROM_NORTH => $this->fromNorth(),
            AnimalPathMove::FROM_WEST => $this->fromWest(),
            AnimalPathMove::FROM_EAST => $this->fromEast(),
            AnimalPathMove::FROM_SOUTH => $this->fromSouth(),
        };

    }

    private function fromWest(): ?AnimalPathMove
    {
        return match($this->symbol) {
            '-' => new AnimalPathMove($this->x+1, $this->y,AnimalPathMove::FROM_WEST),
            'J' => new AnimalPathMove($this->x , $this->y-1,AnimalPathMove::FROM_SOUTH),
            '7'=>  new AnimalPathMove( $this->x, $this->y+1,AnimalPathMove::FROM_NORTH),
            default => null
        };
    }

    private function fromNorth(): ?AnimalPathMove
    {
        return match($this->symbol) {
            '|' => new AnimalPathMove($this->x,$this->y+1,AnimalPathMove::FROM_NORTH),
            'L' =>  new AnimalPathMove($this->x+1 , $this->y,AnimalPathMove::FROM_WEST),
            'J'=>    new AnimalPathMove($this->x-1 , $this->y,AnimalPathMove::FROM_EAST),
            default => null
        };
    }



    public function isDead(): bool
    {
        return $this->symbol === '.';
    }

    public function isStart(): bool
    {
        return $this->symbol === 'S';
    }

    private function fromEast(): ?AnimalPathMove
    {
        return match($this->symbol) {
        'F' => new AnimalPathMove($this->x , $this->y+1,AnimalPathMove::FROM_NORTH),
        '-' => new AnimalPathMove($this->x-1 , $this->y,AnimalPathMove::FROM_EAST),
        'L'=>   new AnimalPathMove($this->x , $this->y-1, AnimalPathMove::FROM_SOUTH),
        default => null
    };
    }

    private function fromSouth(): ?AnimalPathMove
    {
        return match($this->symbol) {
            'F' => new AnimalPathMove($this->x+1 , $this->y, AnimalPathMove::FROM_WEST),
            '|' => new AnimalPathMove($this->x , $this->y-1, AnimalPathMove::FROM_SOUTH),
            '7'=>  new AnimalPathMove( $this->x-1 , $this->y, AnimalPathMove::FROM_EAST),
            default => null
        };
    }

}