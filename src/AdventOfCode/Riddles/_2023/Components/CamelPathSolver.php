<?php

namespace AdventOfCode\Riddles\_2023\Components;

use AdventOfCode\Classes\Math;
use AdventOfCode\Exception\AdventOfCodeException;

class CamelPathSolver
{
    /** @var CamelPath[] */
    private array $paths = [];
    private array $steps;

    public static function fromLines(array $lines): CamelPathSolver
    {
        $instance = new self();
        $steps = array_shift($lines);
        $instance->steps = str_split($steps);
        array_shift($lines);
        foreach ($lines as $line) {
            $path = CamelPath::fromString($line);
            $instance->paths[$path->getName()] = $path;
        }
        foreach ($instance->paths as $path) {
            $path->setLeftPath($instance->paths[$path->getLeftName()]);
            $path->setRightPath($instance->paths[$path->getRightName()]);
        }
        return $instance;
    }

    /**
     * @throws AdventOfCodeException
     */
    public function calcResult(): int
    {

        $curPosition = $this->paths['AAA'];
        $cnt = 0;
        while (!$curPosition->isEnd()) {
            foreach ($this->steps as $direction) {
                $curPosition = $curPosition->move($direction);
                $cnt++;
                if ($curPosition->isEnd()) {
                    return $cnt;
                }
            }
        }


        return 0;
    }

    /**
     * @throws AdventOfCodeException
     */
    public function calcResult2(): int
    {
        $curPositions = array_filter($this->paths, fn(CamelPath $path) => $path->isGhostStart());
        $totalCnt = [];
        foreach ($curPositions as $curPosition) {

            $totalCnt[] = $this->calcSingleGhost($curPosition);
        }
        return Math::calculateLCM($totalCnt);


    }


    /**
     * @throws AdventOfCodeException
     */
    public function calcSingleGhost(CamelPath $curPosition): int
    {
        $cntByPath = 0;
        while (!$curPosition->isGhostEnd()) {
            foreach ($this->steps as $direction) {
                $curPosition = $curPosition->move($direction);
                $cntByPath++;
                if ($curPosition->isGhostEnd()) {
                    return $cntByPath;
                }
            }
        }
        return 0;
    }


}