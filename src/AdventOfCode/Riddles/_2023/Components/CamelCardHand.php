<?php

namespace AdventOfCode\Riddles\_2023\Components;

use AdventOfCode\Exception\AdventOfCodeException;

class CamelCardHand
{
    private int $bid;
    private string $cards;
    private array $cardRanks = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'];
    private int $value;
    private bool $useJokers;
    private const  FIVE_OF_A_KIND = 50;
    private const  FOUR_OF_A_KIND = 40;
    private const  FULL_HOUSE = 32;
    private const  THREE_OF_A_KIND = 30;
    private const  TWO_PAIRS = 22;
    private const  ONE_PAIR = 20;
    private const  HIGH_CARD = 0;


    /**
     * @throws AdventOfCodeException
     */
    public function __construct(int $bid, string $cards, bool $useJokers = false)
    {
        $this->bid = $bid;
        $this->cards = $cards;
        $this->useJokers = $useJokers;

        $ranks = $this->getRanks($cards);
        $this->value = $this->match($ranks);
        if ($useJokers && ($jokerCnt = substr_count($cards, 'J')) > 0) {
            for ($i = 0; $i < $jokerCnt; $i++) {
                $this->updateRankBySingleJoker();
            }
        }


    }

    /**
     * @throws AdventOfCodeException
     */
    private function updateRankBySingleJoker(): void
    {
        $this->value = match ($this->value) {
            self::HIGH_CARD => self::ONE_PAIR,
            self::ONE_PAIR => self::THREE_OF_A_KIND,
            self::TWO_PAIRS => self::FULL_HOUSE,
            self::THREE_OF_A_KIND, self::FULL_HOUSE => self::FOUR_OF_A_KIND,
            self::FOUR_OF_A_KIND,self::FIVE_OF_A_KIND => self::FIVE_OF_A_KIND,
            default => throw new AdventOfCodeException('Unexpected value ' . $this->value . ' for ' . $this->cards . ' with joker'),
        };


    }

    public function match($ranks): int
    {
        return match (true) {
            ($ranks[5] ?? 0) > 0 => self::FIVE_OF_A_KIND,
            ($ranks[4] ?? 0) > 0 => self::FOUR_OF_A_KIND,
            ($ranks[3] ?? 0) > 0 && ($ranks[2] ?? 0) > 0 => self::FULL_HOUSE,
            ($ranks[3] ?? 0) > 0 => self::THREE_OF_A_KIND,
            ($ranks[2] ?? 0) > 1 => self::TWO_PAIRS,
            ($ranks[2] ?? 0) > 0 => self::ONE_PAIR,
            default => self::HIGH_CARD,
        };
    }

    public function compare(CamelCardHand $second): int
    {
        if (($diff = $this->value - $second->value) !== 0) {
            return $diff;
        }
        $cardRanks = $this->getCardRanks();
        if($this->useJokers) {
            $cardRanks[] = 'J';
        }
        foreach (str_split($this->cards) as $idx => $card) {
            if ($card !== $second->cards[$idx]) {
                return array_search($second->cards[$idx], $cardRanks) - array_search($card, $cardRanks);
            }
        }
        return 0;
    }

    public function getCards(): string
    {
        return $this->cards;
    }

    public function getBid(): int
    {
        return $this->bid;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param string $cards
     * @return array
     */
    public function getRanks(string $cards): array
    {
        $ranks = [];
        foreach ($this->getCardRanks() as $rank) {
            if (($cnt = substr_count($cards, $rank)) > 1) {
                $ranks[$cnt] ??= 0;
                $ranks[$cnt]++;
            }

        }
        return $ranks;
    }

    /**
     * @return array|string[]
     */
    public function getCardRanks(): array
    {
        return $this->useJokers ? array_diff($this->cardRanks, ['J']) : $this->cardRanks;
    }

}