<?php

namespace AdventOfCode\Riddles\_2023\Components;

enum Cube: string
{
    case RED = 'red';
    case GREEN = 'green';
    case BLUE = 'blue';
}

