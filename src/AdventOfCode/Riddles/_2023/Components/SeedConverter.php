<?php

namespace AdventOfCode\Riddles\_2023\Components;

class SeedConverter
{
    /** @var  SeedArea[] */
    private array $seedAreas = [];
    private ?SeedConverter $seedConverter=null;

    public function addArea(SeedArea $area): void
    {
        $this->seedAreas[] = $area;
    }

    public function setSeedConverter(SeedConverter $seedConverter): void
    {
        $this->seedConverter = $seedConverter;
    }

    public function getLocation(int $seed): int
    {
        $target = $this->getTarget($seed);
        return $this->seedConverter?->getLocation($target) ?? $target;
    }

    public function getTarget(int $seed): int
    {
        foreach ($this->seedAreas as $seedArea) {
            if ($seedArea->isInRange($seed)) {
                return $seedArea->getMappingForSeed($seed);
            }
        }
        return $seed;
    }

}