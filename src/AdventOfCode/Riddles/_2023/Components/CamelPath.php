<?php

namespace AdventOfCode\Riddles\_2023\Components;

use AdventOfCode\Exception\AdventOfCodeException;

class CamelPath
{
//LHF = (QTF, KKT)

    private string $name;
    private string $leftName;
    private string $rightName;
    private CamelPath $leftPath;
    private CamelPath $rightPath;

    /**
     * LHF = (QTF, KKT)
     */
    public static function fromString(string $input): CamelPath
    {
        [$name, $leftRight] = explode(' = ', $input);
        [$left, $right] = explode(', ', trim($leftRight, '()'));
        return new self($name, $left, $right);
    }

    private function __construct(string $name, string $left, string $right)
    {
        $this->name = $name;
        $this->leftName = $left;
        $this->rightName = $right;
    }

    public function setLeftPath(CamelPath $leftPath): void
    {
        $this->leftPath = $leftPath;
    }

    public function setRightPath(CamelPath $rightPath): void
    {
        $this->rightPath = $rightPath;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function getLeftName(): string
    {
        return $this->leftName;
    }

    public function getRightName(): string
    {
        return $this->rightName;
    }

    public function isGhostStart(): bool
    {
        return str_ends_with($this->name, 'A');
    }

    public function isEnd(): bool
    {
        return $this->name === 'ZZZ';
    }

    public function isGhostEnd(): bool
    {
        return str_ends_with($this->name, 'Z');
    }


    /**
     * @throws AdventOfCodeException
     */
    public function move(string $direction): CamelPath
    {
        if ($direction === 'L') {
            return $this->leftPath;
        }
        if ($direction === 'R') {
            return $this->rightPath;
        }
        throw new AdventOfCodeException('Unknown direction: ' . $direction);
    }




}