<?php

namespace AdventOfCode\Riddles\_2023\Components;

class AnimalPathMove
{

    private int $x;
    private int $y;
    private int $from;

    public const FROM_NORTH = 2;
    public const FROM_WEST = 4;
    public const FROM_EAST = 6;
    public const FROM_SOUTH = 8;
    /**
     * @param int $x
     * @param int $y
     * @param int $from
     */
    public function __construct(int $x, int $y, int $from)
    {
        $this->x = $x;
        $this->y = $y;
        $this->from = $from;
    }

    public function getFrom(): int
    {
        return $this->from;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }






}