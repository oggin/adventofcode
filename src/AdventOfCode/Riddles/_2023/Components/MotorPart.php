<?php

namespace AdventOfCode\Riddles\_2023\Components;

class MotorPart
{
    /**
     * @var MotorPartPoint[]
     */
    private array $points = [];
    private string $id = '';
    private string $uid;
    private bool $finished = false;


    public function __construct()
    {
        $this->uid = uniqid();
    }

    public function update(MotorPartPoint $point): void
    {
        $this->points[] = $point;
        $this->id .= $point->getChar();
    }

    public function isPart(MotorPartPoint ...$symbols): bool
    {
        foreach ($symbols as $symbol) {
            foreach ($this->points as $point) {
                if ($point->isAdjacent($symbol)) {
                    return true;
                }
            }

        }
        return false;
    }

    public function getPartSum(MotorPartPoint ...$motorPartPoints): int
    {
        return $this->isPart(... $motorPartPoints) ? $this->getId() : 0;

    }

    public function getId(): int
    {
        return (int)$this->id;
    }

    public function getPoints(): array
    {
        return $this->points;
    }

    public function updateAsteriskAdjacents(MotorPartPoint $asterisk): void
    {
        foreach ($this->points as $point) {
            if ($point->isAdjacent($asterisk)) {
                $asterisk->addAdjacent($this);
                return;
            }
        }
    }

    public function isFinished(): bool
    {
        return $this->finished;
    }

    public function setFinished(bool $finished): void
    {
        $this->finished = $finished;
    }

    public function getUid(): string
    {
        return $this->uid;
    }






}