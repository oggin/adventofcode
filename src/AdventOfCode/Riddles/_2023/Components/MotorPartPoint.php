<?php

namespace AdventOfCode\Riddles\_2023\Components;

class MotorPartPoint
{
    private int $x;
    private int $y;
    private string $char;
    /**
     * @var MotorPart[]
     */
    private array $adjacent = [];


    public function __construct(int $x, int $y, string $char)
    {
        $this->x = $x;
        $this->y = $y;
        $this->char = $char;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function isAsterisk(): bool
    {
        return $this->char === '*';
    }

    public function isAdjacent(MotorPartPoint $point): bool
    {
        /**
         *  .x....
         *  x1x34.
         *  .x....
         */

        $px = $point->getX();
        $py = $point->getY();
        $symbolX = $this->x;
        $symbolY = $this->y;

        return $px === $symbolX && $py === $symbolY + 1
            || $px === $symbolX && $py === $symbolY - 1
            || $px === $symbolX + 1 && $py === $symbolY
            || $px === $symbolX - 1 && $py === $symbolY
            || $px === $symbolX + 1 && $py === $symbolY + 1
            || $px === $symbolX + 1 && $py === $symbolY - 1
            || $px === $symbolX - 1 && $py === $symbolY + 1
            || $px === $symbolX - 1 && $py === $symbolY - 1;

    }

    public function addAdjacent(MotorPart $part): void
    {
        $this->adjacent['#'.$part->getId()] = $part;
    }

    public function getAdjacentProduct(): float|int
    {
        if (count($this->adjacent) !== 2) {
            return 0;
        }

        return reset($this->adjacent)->getId() * end($this->adjacent)->getId();

    }

    public function isDot(): bool
    {
        return $this->char === '.';
    }

    public function isSymbol(): bool
    {
        return !is_numeric($this->char) && !$this->isDot();
    }

    public function getChar(): int
    {
        return (int)$this->char;
    }


    public function updateAsteriskAdjacents(MotorPart ...$motorParts): void
    {
        foreach ($motorParts as $motorPart) {
            $motorPart->updateAsteriskAdjacents($this);
        }

}

}