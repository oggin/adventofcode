<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2023\Components\MotorPartSolver;

class Day3 extends MainRiddle
{
    public static int $day = 3;
    public static int $year = 2023;

    public function __construct()
    {
        parent::__construct();

    }

    public function calcResult(): int
    {
        return (new MotorPartSolver($this->getLines()))->calcResult();
    }

    public function calcResult2(): int
    {
        return (new MotorPartSolver($this->getLines()))->calcResult2();
    }

}
