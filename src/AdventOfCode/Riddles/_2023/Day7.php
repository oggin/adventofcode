<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Exception\AdventOfCodeException;
use AdventOfCode\Riddles\_2023\Components\CamelCardHand;

class Day7 extends MainRiddle
{
    public static int $day = 7;
    public static int $year = 2023;

    public function calcResult(): int
    {
        return $this->solve(false);
    }

    public function calcResult2(): int
    {
        return $this->solve(true);
    }

    /**
     * @throws AdventOfCodeException
     */
    public function solve(bool $useJokers): int|float
    {
        /** @var CamelCardHand[] $cards */
        $cards = [];
        foreach ($this->lines as $line) {
            [$cardPart, $bid] = explode(' ', $line);
            $cards[] = new CamelCardHand((int)$bid, $cardPart, $useJokers);
        }
        usort($cards, fn(CamelCardHand $a, CamelCardHand $b) => $a->compare($b));

        return array_sum(
            array_map(
                static fn(CamelCardHand $card, int $idx) => $card->getBid() * ($idx + 1),
                array_values($cards), array_keys($cards))
        );

    }

}
