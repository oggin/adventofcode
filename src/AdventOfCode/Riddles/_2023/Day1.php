<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2023\Components\StringNumbers;

class Day1 extends MainRiddle
{
    public static int $day = 1;
    public static int $year = 2023;

    public function calcResult(): int
    {
        $total = 0;
        foreach ($this->lines as $line) {
            $total += StringNumbers::fromString($line)->getNumberSimple();
        }
        return $total;
    }

    public function calcResult2(): int
    {

        StringNumbers::fromString('3369eightnine89')->getNumber();

        $total = 0;
        foreach ($this->lines as $line) {
          //  echo $line . ' - ' . StringNumbers::fromString($line)->getNumber() . PHP_EOL;
            $total += StringNumbers::fromString($line)->getNumber();
        }
        return $total;
//53513
    }

}
