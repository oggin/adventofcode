<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2023\Components\AnimalPath;
use AdventOfCode\Riddles\_2023\Components\AnimalPathMove;

class Day10 extends MainRiddle
{
    public static int $day = 10;
    public static int $year = 2023;

    public function calcResult(): int
    {
        $animalPaths=[];
        foreach ($this->lines as $y => $line) {
            foreach (str_split($line) as $x => $symbol) {
                $animalPaths = new AnimalPath($x, $y, $symbol, count($this->lines));
            }
        }

        $start = array_filter($animalPaths, function (AnimalPath $animalPath) {
            return $animalPath->isStart() ;
        });
        /**
         * @var AnimalPath $start
         */
        $cur = $start;
        foreach ([AnimalPathMove::FROM_NORTH,AnimalPathMove::FROM_WEST, AnimalPathMove::FROM_EAST, AnimalPathMove::FROM_SOUTH] as $move) {
            $move = new AnimalPathMove($start->getX(), $start->getY(), $move);
            while(($move = $cur->getPositionNext($move)) !== null) {
                $cur = array_filter($animalPaths, function (AnimalPath $animalPath) {
                    return $animalPath->isStart() ;
                });
            }
            $start = $start->getPositionNext();
        }
        return 10 * 10;
    }

    public function calcResult2(): int
    {
         return 10 * 10;
    }

}
