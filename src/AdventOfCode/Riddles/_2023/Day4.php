<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2023\Components\Card;

class Day4 extends MainRiddle
{
    public static int $day = 4;
    public static int $year = 2023;

    public function calcResult(): int
    {
        return array_sum(array_map(static fn(string $line) => Card::fromLine($line)->getScore(), $this->lines));
    }

    public function calcResult2(): int
    {

        $cards = [];
        foreach ($this->lines as $line) {
            $cards[] = Card::fromLine($line);
        }

        for ($mainIdx = 0; $mainIdx < count($cards); $mainIdx++) {
            for ($toAdd = 1; $toAdd <= $cards[$mainIdx]->getMatchCount(); $toAdd++) {
                $cards[$mainIdx + $toAdd]?->increaseAmountBy($cards[$mainIdx]->getAmount());
            }
        }

        return array_sum(array_map(static fn(Card $card) => $card->getAmount(), $cards));
    }
}
