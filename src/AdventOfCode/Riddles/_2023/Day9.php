<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2023\Components\OasisSensor;

class Day9 extends MainRiddle
{
    public static int $day = 9;
    public static int $year = 2023;

    public function calcResult(): int
    {
        $sum = 0;
        foreach ($this->lines as $line) {
            $sum += (new OasisSensor($line))->getNextValue();
        }
        return $sum;

    }

    public function calcResult2(): int
    {
        $sum = 0;
        foreach ($this->lines as $line) {

            $sum += (new OasisSensor($line))->getPrevValue();
        }
        return $sum;
    }

}
