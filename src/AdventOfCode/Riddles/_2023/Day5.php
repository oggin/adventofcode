<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\Helper;
use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2023\Components\SeedArea;
use AdventOfCode\Riddles\_2023\Components\SeedConverter;

class Day5 extends MainRiddle
{
    public static int $day = 5;
    public static int $year = 2023;

    public function calcResult(): int
    {
        $areas = preg_split('~\n\n~', trim($this->content));
        $seedsArea = array_shift($areas);
        [, $seedsAsString] = explode(': ', $seedsArea);
        $seeds = Helper::castElementsToInt(explode(' ', $seedsAsString));
        $mainSeedConverter = new SeedConverter();
        $parentConverter = $mainSeedConverter;
        foreach ($areas as $area) {

            $converter = new SeedConverter();
            $parentConverter->setSeedConverter($converter);
            foreach (explode("\n", preg_split("~:\n~", $area)[1]) as $dataLine) {
                $converter->addArea(new SeedArea(... Helper::castElementsToInt(explode(' ', $dataLine))));
            }
            $parentConverter = $converter;

        }

        return min(array_map(static fn(int $seed) => $mainSeedConverter->getLocation($seed), $seeds));


    }

    public function calcResult2(): int
    {
        return 1;
        $areas = preg_split('~\n\n~', trim($this->content));
        $seedsArea = array_shift($areas);
        [, $seedsAsString] = explode(': ', $seedsArea);
        $seeds = Helper::castElementsToInt(explode(' ', $seedsAsString));
        $mainSeedConverter = new SeedConverter();
        $parentConverter = $mainSeedConverter;
        foreach ($areas as $area) {

            $data = preg_split("~:\n~", $area);


            $converter = new SeedConverter();
            $parentConverter->setSeedConverter($converter);
            foreach (explode("\n", $data[1]) as $dataLine) {
                [$to, $from, $length] = explode(' ', $dataLine);
                $converter->addArea(new SeedArea((int)$to, (int)$from, (int)$length));
            }
            $parentConverter = $converter;
            var_dump($data);
        }
        $allMins = [];
        for ($idx =0; $idx < count($seeds); $idx+=2){
            $curMin = [];
            for($seed = $seeds[$idx]; $seed < $seeds[$idx] + $seeds[$idx+1]; $seed++){
                $curMin[] = $mainSeedConverter->getLocation($seed);
            }
            $allMins[] = min($curMin);

        }
        return min($allMins);

    }

}
