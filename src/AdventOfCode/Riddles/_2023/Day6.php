<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2023;

use AdventOfCode\Classes\MainRiddle;

class Day6 extends MainRiddle
{
    public static int $day = 6;
    public static int $year = 2023;

    public function calcResult(): int
    {
        $times = preg_split('~\s+~', $this->lines[0]);
        $distances = preg_split('~\s+~', $this->lines[1]);
        array_shift($times);
        array_shift($distances);

        $total = 1;
        foreach ($times as $idx => $value) {
            $total *= $this->getCnt((int)$value, (int)$distances[$idx]);
        }

        return $total;
    }

    public function calcResult2(): int
    {
        $times = preg_split('~\s+~', $this->lines[0]);
        $distances = preg_split('~\s+~', $this->lines[1]);
        array_shift($times);
        array_shift($distances);

        return $this->getCnt((int)implode('', $times), (int)implode('', $distances));
    }

    /**
     * @param int $time
     * @param int $distanceToBeat
     * @return int
     */
    public function getCnt(int $time, int $distanceToBeat): int
    {
        $cnt = 0;
        for ($speed = 1; $speed < $time; $speed++) {

            $dist = $time - $speed;
            if ($speed * $dist > $distanceToBeat) {
                $cnt++;
            } elseif ($cnt > 0) {
                break;
            }
        }
        return $cnt;
    }

}
