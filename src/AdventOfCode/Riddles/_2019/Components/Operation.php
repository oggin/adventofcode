<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2019\Components;


use AdventOfCode\Exception\AdventOfCodeException;

class Operation
{
    public const OP_SUM = 1;
    public const OP_MULTIPLY = 2;
    public const OP_STOP = 99;
    private int $operation;
    private ?int $firstNumber = null;
    private ?int $secondNumber = null;


    public function __construct(int $opCode, ?int $firstNumber, ?int $secondNumber)
    {
        $this->operation = in_array($opCode, [self::OP_SUM, self::OP_MULTIPLY, self::OP_STOP]) ?
            $opCode : throw new AdventOfCodeException("Invalid opcode: $opCode");

        $this->firstNumber = $firstNumber;
        $this->secondNumber = $secondNumber;

    }

    public function isFinished():bool
    {
        return $this->operation === self::OP_STOP;
    }

    public function calculate(): float|int|null
    {
       return match($this->operation){
           self::OP_SUM => $this->firstNumber + $this->secondNumber,
           self::OP_MULTIPLY => $this->firstNumber * $this->secondNumber,
           default =>throw new AdventOfCodeException("calculation - nvalid opcode $this->opcode")
       };
    }
}