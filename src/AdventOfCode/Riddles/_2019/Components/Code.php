<?php

namespace AdventOfCode\Riddles\_2019\Components;

use AdventOfCode\Classes\Helper;

class Code
{

    private array $digits;

    public static function fromNumber(int $number): Code
    {
        $code = new Code();
        $code->digits = Helper::castElementsToInt(str_split((string)$number));
        return $code;
    }

    public function match(): bool
    {
        $prev = 0;
        $hasDouble = false;
        foreach ($this->digits as $digit) {
            if ($digit < $prev) {
                return false;
            }
            $hasDouble = $hasDouble || $digit === $prev;
            $prev = $digit;
        }
        return $hasDouble;
    }

    public function matchTwo(): bool
    {
        $hasRealDouble = false;
        foreach ($this->digits as $idx => $digit) {
            $prev = $this->digits[$idx - 1] ?? 0;
            if ($idx > 0 && $digit < $prev) {
                return false;
            }
            if ($idx > 0 && $idx < 5 && !$hasRealDouble) {
                $hasRealDouble = $digit === $prev && $digit !== $this->digits[$idx + 1];
            }
            if (!$hasRealDouble && $idx === 5) {
                $hasRealDouble = $digit === $prev;
            }

        }
        return $hasRealDouble;
    }

}