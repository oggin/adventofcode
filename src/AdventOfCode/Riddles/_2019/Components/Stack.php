<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2019\Components;

use AdventOfCode\Classes\Helper;

class Stack
{
    private array $data = [];

    public function __construct(string $data)
    {
        $numbers = Helper::castElementsToInt(explode(',', $data));
        foreach ($numbers as $idx => $number) {
            $this->data['#' . $idx] = $number;
        }
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getSize(): int
    {
        return count($this->data);
    }

    public function getNumberByIndex(int $index): ?int
    {
        return $this->data['#' . $index] ?? null;
    }

    public function setNumberByIndex(int $index, int $number): ?int
    {
        return $this->data['#' . $index] = $number;
    }
}