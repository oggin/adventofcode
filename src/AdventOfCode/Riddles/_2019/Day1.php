<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2019;

use AdventOfCode\Classes\MainRiddle;

class Day1 extends MainRiddle
{
    public static int $day = 1;
    public static int $year = 2019;

    public function calcResult(): int
    {
        return $this->calcFuel(false);
    }

    public function calcResult2(): int
    {
        return $this->calcFuel(true);
    }

    private function calcFuel(bool $recursive): int
    {
        $total = 0;
        foreach ($this->getLinesAsInt() as $line) {
            $total += $recursive ? $this->calcRecursive($line) : $this->calcSingleFuel($line);
        }
        return $total;
    }

    public function calcRecursive(int $line): int
    {
        $recursiveFuel = 0;
        $fuelLeft = $line;
        while (($curlFuel = $this->calcSingleFuel($fuelLeft)) > 0) {
            $recursiveFuel += $curlFuel;
            $fuelLeft = $curlFuel;
        }

        return $recursiveFuel;
    }

    public function calcSingleFuel(int $line): int
    {
        return max(((int)($line / 3) - 2), 0);
    }

}
