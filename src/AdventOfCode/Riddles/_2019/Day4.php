<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2019;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2019\Components\Code;

class Day4 extends MainRiddle
{
    public static int $day = 4;
    public static int $year = 2019;

    public function calcResult(): int
    {
        var_dump($this->lines);
        [$from, $to] = explode('-', $this->lines[0]);
        $from = (int)$from;
        $to = (int)$to;
        $count = 0;
        foreach (range($from, $to) as $number) {
            $count += Code::fromNumber($number)->match() ? 1 : 0;
        }
        return $count;
    }

    public function calcResult2(): int
    {
        [$from, $to] = explode('-', $this->lines[0]);
        $from = (int)$from;
        $to = (int)$to;
        $count = 0;
        foreach (range($from, $to) as $number) {
            $count += Code::fromNumber($number)->matchTwo() ? 1 : 0;
        }
        return $count;
    }

}
