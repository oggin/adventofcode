<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2019;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2019\Components\Operation;
use AdventOfCode\Riddles\_2019\Components\Stack;

class Day2 extends MainRiddle
{
    public static int $day = 2;
    public static int $year = 2019;

    public function calcResult(): int
    {
        return $this->getStackAtIndexZero();
    }

    public function calcResult2(): int
    {
        for ($noun = 0; $noun <= 99; $noun++) {
            for ($verb = 0; $verb <= 99; $verb++) {
                if ($this->getStackAtIndexZero($noun, $verb) === 19690720) {
                    return (int)('' . $noun . $verb);
                }
            }
        }
        return -1;
    }
    
    public function getStackAtIndexZero(int $noun = 12, int $verb = 2): ?int
    {
        $stack = new Stack($this->lines[0]);
        $stack->setNumberByIndex(1, $noun);
        $stack->setNumberByIndex(2, $verb);
        for ($index = 0; $index < $stack->getSize() - 1; $index += 4) {
            $saveIndex = $stack->getNumberByIndex($index + 3);
            $operation = new Operation(
                $stack->getNumberByIndex($index),
                $stack->getNumberByIndex($stack->getNumberByIndex($index + 1)),
                $stack->getNumberByIndex($stack->getNumberByIndex($index + 2)),
            );

            if ($operation->isFinished()) {
                return $stack->getNumberByIndex(0);
            }

            $stack->setNumberByIndex($saveIndex, $operation->calculate());


        }
        return $stack->getNumberByIndex(0);
    }

}
