<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022\Components\Day21;

use AdventOfCode\Exception\AdventOfCodeException;

class Monkey
{
    private string $name;
    private ?int $result = null;
    private ?string $operator = null;
    private ?int $firstNumber = null;
    private ?int $secondNumber = null;
    private ?string $firstDep = null;
    private ?string $secondDep = null;

    private ?Monkey $firstDepMonkey = null;
    private ?Monkey $secondDepMonkey = null;

    public static function fromLine(string $line): self
    {
        $instance = new self();
        $parts = explode(':', $line);
        $instance->name = trim($parts[0]);

        $dataParts = explode(' ', trim($parts[1]));
        if ($first = array_shift($dataParts)) {
            if ($instance->isInt($first)) {
                $instance->firstNumber = (int)$first;
            } else {
                $instance->firstDep = $first;
            }
        }

        if (empty($dataParts) && is_int($instance->firstNumber)) {
            $instance->result = $instance->firstNumber;
        }

        if ($operator = array_shift($dataParts)) {
            $instance->operator = trim($operator);
        }

        if ($second = array_shift($dataParts)) {
            if ($instance->isInt($second)) {
                $instance->secondNumber = (int)$second;
            } else {
                $instance->secondDep = $second;
            }
        }

        return $instance;
    }

    private function isInt(string $value): bool
    {
        return (int)$value !== 0 || $value === '0';
    }


    /**
     * @throws AdventOfCodeException
     */
    public function getFirstNumber(): float
    {
        return $this->firstNumber ?? $this->firstDepMonkey->getResult();
    }

    /**
     * @throws AdventOfCodeException
     */
    public function getSecondNumber(): int
    {
        return $this->secondNumber ?? $this->secondDepMonkey->getResult();
    }

    public function setResult(int $result): void
    {
        $this->result = $result;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @throws AdventOfCodeException
     */
    public function getResult(): int
    {
        return $this->result ?? $this->calcResults();
    }

    /**
     * @throws AdventOfCodeException
     */
    public function calcBack(Monkey $targetMonkey, int|float $expectedResult = null): void
    {

        if ($this->result) {
            if ($this->getName() === $targetMonkey->getName()) {
                $targetMonkey->setResult($expectedResult);
            }
            return;
        }

        if($expectedResult === null){
            $expectedResultFirst = $this->getSecondNumber();
            $expectedResult = $this->getSecondNumber();
        }
        else{

        $expectedResultFirst = $this->calcByOperator($this->getOppositeOperatorFirst(), $expectedResult, $this->secondDepMonkey->getResult());

    }

        $expectedResultSecond = $this->calcSecond($expectedResult);
        $this->firstDepMonkey->calcBack($targetMonkey, $expectedResultFirst);
        $this->secondDepMonkey->calcBack($targetMonkey, $expectedResultSecond);
    }


    /**
     * @throws AdventOfCodeException
     */
    private function calcSecond(float $expected):float {
        $firstDepResult = $this->firstDepMonkey->getResult();
        return match ($this->operator) {
            '-'=> $firstDepResult - $expected,
            '+'=> $expected-$firstDepResult,
            '*'=> $expected / $firstDepResult,
            '/'=> $firstDepResult / $expected
        };
    }
    /**
     * @throws AdventOfCodeException
     */
    public function getOppositeOperatorFirst(): string
    {
        return match ($this->operator) {
            '+' => '-',
            '-' => '+',
            '*' => '/',
            '/' => '*',
            default => throw new AdventOfCodeException('invalid operator ' . $this->operator)
        };
    }

    /**
     * @throws AdventOfCodeException
     */
    public function calcResults(): int
    {
        $first = $this->firstDepMonkey->getResult();
        $second = $this->secondDepMonkey->getResult();
        $operator = $this->operator;
        return $this->calcByOperator($operator, $first, $second);

    }

    /**
     * @throws AdventOfCodeException
     */
    public function resolveDep(MonkeyStack $stack): void
    {
        $this->firstDepMonkey = $this->firstDep ? $stack->getMonkey($this->firstDep) : null;
        $this->secondDepMonkey = $this->secondDep ? $stack->getMonkey($this->secondDep) : null;
    }

    /**
     * @throws AdventOfCodeException
     */
    public function calcByOperator(string $operator, float $first, float $second): int
    {
        $ret = match ($operator) {
            '+' => $first + $second,
            '-' => $first - $second,
            '*' => $first * $second,
            '/' => $first / $second,
            default => throw new AdventOfCodeException('invalid operator ' . $operator)
        };


        return (int)$ret;
    }


}