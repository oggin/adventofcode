<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022\Components\Day21;

use AdventOfCode\Exception\AdventOfCodeException;

class MonkeyStack
{

    /** @var Monkey[] */
    private array $monkeys = [];

    /**
     * @throws AdventOfCodeException
     */
    public function __construct(array $lines)
    {
        foreach ($lines as $line) {
            $monkey = Monkey::fromLine($line);
            $this->add($monkey);

        }
        foreach ($this->monkeys as $monkey) {
            $monkey->resolveDep($this);
        }
    }

    public function add(Monkey $monkey): void
    {
        $this->monkeys[$monkey->getName()] = $monkey;
    }


    /**
     * @throws AdventOfCodeException
     */
    public function getMonkey(string $key): Monkey
    {
        return $this->monkeys[$key] ?? throw new AdventOfCodeException('not on stack');
    }

}