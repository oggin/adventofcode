<?php

namespace AdventOfCode\Riddles\_2022\Components\Day8;

class TreeHouseGrid
{
    private array $stack;

    private int $size;


    public function __construct(int $size)
    {
        $this->size = $size;
        $this->stack = array_fill(0, $size, 0);
        foreach (array_keys($this->stack) as $idx) {
            $this->stack[$idx] = array_fill(0, $size, 0);
        }
    }

    public function addElement(int $x, int $y, int $element): void
    {
        $this->stack[$x][$y] = $element;
    }

    public function getVisibleFromOutsideCount(): int
    {
        $visibleCount = 0;
        foreach ($this->stack as $rowIdx => $value) {
            foreach ($this->stack[$rowIdx] as $colIdx => $value1) {
                if ($this->isOuter($rowIdx, $colIdx)) {
                    $visibleCount++;
                    continue;
                }




                $visibleCount += ($this->isVisibleByRow($rowIdx, $colIdx) || $this->isVisbleByCol($rowIdx, $colIdx)) ? 1 : 0;
            }
        }
        return $visibleCount;
    }

    public function getHighestScenicScore(): int
    {
        $highestScrore = 0;
        for ($rowIdx = 0; $rowIdx < $this->size; $rowIdx++) {
            for ($colIdx = 0; $colIdx < $this->size; $colIdx++) {
                if ($this->isOuter($rowIdx, $colIdx)) {
                    continue;
                }
                $highestScrore = (int)max($highestScrore, $this->scenic($rowIdx, $colIdx));
            }
        }

        return $highestScrore;
    }


    private function isVisibleByRow(int $staticRowIdx, int $coIdxToCheck): bool
    {
        $valToCheck = $this->getPoint($staticRowIdx, $coIdxToCheck);

        return $this->isVisibleLeft($coIdxToCheck, $staticRowIdx, $valToCheck) || $this->isVisibleRight($coIdxToCheck, $staticRowIdx, $valToCheck);
    }

    private function isVisbleByCol(int $rowIdxToCheck, int $staticColIdx): bool
    {
        $valToCheck = $this->getPoint($rowIdxToCheck, $staticColIdx);
        return $this->isVisibleUp($rowIdxToCheck, $staticColIdx, $valToCheck) || $this->isVisibleDown($rowIdxToCheck, $staticColIdx, $valToCheck);
    }

    private function isOuter(int $x, int $y): bool
    {
        return $x === 0 || $x === $this->size - 1 || $y === 0 || $y === $this->size - 1;
    }

    private function isVisibleLeft(int $coIdxToCheck, int $staticRowIdx, mixed $valToCheck): bool
    {
        for ($curColIdx = 0; $curColIdx < $coIdxToCheck; $curColIdx++) {
            if ($this->getPoint($staticRowIdx, $curColIdx) >= $valToCheck) {
                return false;
            }
        }

        return true;
    }


    private function isVisibleRight(int $coIdxToCheck, int $staticRowIdx, mixed $valToCheck): bool
    {
        for ($curColIdx = $this->size - 1; $curColIdx > $coIdxToCheck; $curColIdx--) {
            if ($this->getPoint($staticRowIdx, $curColIdx) >= $valToCheck) {
                return false;
            }
        }
        return true;
    }

    private function isVisibleUp(int $rowIdxToCheck, int $staticColIdx, mixed $valToCheck): bool
    {
        for ($curRowIdx = 0; $curRowIdx < $rowIdxToCheck; $curRowIdx++) {
            if ($this->getPoint($curRowIdx, $staticColIdx) >= $valToCheck) {
                return false;
            }
        }
        return true;
    }

    private function isVisibleDown(int $rowIdxToCheck, int $staticColIdx, mixed $valToCheck): bool
    {
        for ($curRowIdx = $this->size - 1; $curRowIdx > $rowIdxToCheck; $curRowIdx--) {
            if ($this->getPoint($curRowIdx, $staticColIdx) >= $valToCheck) {
                return false;
            }
        }
        return true;
    }


    private function getPoint(int $curRowIdx, int $staticColIdx): int
    {
        return $this->stack[$curRowIdx][$staticColIdx];
    }


    private function scenic($givenRowIdx, $givenColIdx): int
    {
        $height = $this->getPoint($givenRowIdx, $givenColIdx);
        $north = $south = $east = $west = 0;

        $stacksize = count($this->stack);
        for ($curRowIdx = $givenRowIdx + 1; $curRowIdx < $stacksize; $curRowIdx++) {
            $south++;

            if ($this->getPoint($curRowIdx, $givenColIdx) >= $height) {
                break;
            }
        }
        for ($rowIdx = $givenRowIdx - 1; $rowIdx >= 0; $rowIdx--) {
            $north++;
            if ($this->getPoint($rowIdx, $givenColIdx) >= $height) {
                break;
            }
        }
        for ($urColIdx = $givenColIdx + 1; $urColIdx < $stacksize; $urColIdx++) {
            $east++;

            if ($this->getPoint($givenRowIdx, $urColIdx) >= $height) {
                break;
            }
        }
        for ($urColIdx = $givenColIdx - 1; $urColIdx >= 0; $urColIdx--) {
            $west++;
            if ($this->getPoint($givenRowIdx, $urColIdx) >= $height) {
                break;
            }
        }

        return $south * $north * $east * $west;
    }
}
