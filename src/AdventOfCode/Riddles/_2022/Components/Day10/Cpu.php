<?php

namespace AdventOfCode\Riddles\_2022\Components\Day10;

class Cpu
{
    private int $sum = 0;
    private int $x = 1;
    private int $addX = 0;
    private array $cmdStack;
    private string $crt = '';
    private array $result = [];
    private int $cylceMod = 0;

    /**
     * @param array $cmdStack
     */
    public function __construct(array $cmdStack)
    {
        $this->cmdStack = $cmdStack;
    }


    public function getCycleXProduct(): int
    {

        for ($cycle = 1; $cycle <= 220; $cycle++) {
            $this->checkProductCycle($cycle);
            $this->handleSummation();
        }
        return $this->sum;
    }

    private function checkProductCycle(int $cycle): void
    {
        if ($cycle === 20 || ($cycle - 20) % 40 === 0) {
            $this->sum += $cycle * $this->x;
            // echo $cycle . ' - ' . $this->x . ' - ' . ($cycle * $this->x) . '- ' . $this->sum . PHP_EOL;
        }
    }

    private function checkCRTCycle(int $cycle): void
    {
        if ($cycle % 40 === 0) {
            //echo $this->crt . PHP_EOL;
            $this->result[] = $this->crt;
            $this->cylceMod = -1;
            $this->crt = '';
        }
    }

    public function printCRT(): string
    {

        for ($cycle = 1; $cycle <= 240; $cycle++) {
            $this->drawSprite();
            $this->checkCRTCycle($cycle);
            $this->handleSummation();
            $this->cylceMod++;
        }
        return implode(PHP_EOL, $this->result);
    }


    private function handleSummation(): void
    {
        if ($this->addX === 0) {
            $cmd = array_shift($this->cmdStack);
            if ($cmd[0] === 'addx') {
                $this->addX = $cmd[1];
            }
        } else {
            $this->x += $this->addX;
            $this->addX = 0;
        }
    }

    private function drawSprite(): void
    {
        if ($this->cylceMod >= $this->x - 1 && $this->cylceMod <= $this->x + 1) {
            $this->crt .= '#';
        } else {
            $this->crt .= '.';
        }
    }
}
