<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022\Components\Day9;

class Point
{
    private int $x;
    private int $y;

    /**
     * @param int $x
     * @param int $y
     */
    public function __construct(int $x = 0, int $y = 0)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(): int
    {
        return $this->x;
    }


    public function getY(): int
    {
        return $this->y;
    }

    public function addY(int $add): void
    {
        $this->y += $add;
    }

    public function addX(int $add): void
    {
        $this->x += $add;
    }

    public function equalsX(Point $otherPoint): bool
    {
        return $this->x === $otherPoint->getX();
    }

    public function equalsY(Point $otherPoint): bool
    {
        return $this->y === $otherPoint->getY();
    }


    public function getAbsDiffX(Point $otherPoint): int
    {
        return abs($this->x - $otherPoint->getX());
    }

    public function getAbsDiffY(Point $otherPoint): int
    {
        return abs($this->y - $otherPoint->getY());
    }
}
