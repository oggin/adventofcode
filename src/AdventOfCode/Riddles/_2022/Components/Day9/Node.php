<?php

namespace AdventOfCode\Riddles\_2022\Components\Day9;

class Node
{
    private Point $head;
    private Point $tail;
    private array $posVisited = [];
    private ?Node $child = null;

    private int $minX = 0;
    private int $maxX = 0;
    private int $minY = 0;
    private int $maxY = 0;
    private int $level = 1;


    public function __construct($depth = 1, ?Node $parent = null)
    {
        $this->head = new Point();
        $this->tail = new Point();
        $this->level = $parent ? $parent->level + 1 : 1;
        if ($depth > 1) {
            $this->child = new Node($depth - 1, $this);
        }
    }

    public function getHead(): Point
    {
        return $this->head;
    }


    public function getTail(): Point
    {
        return $this->tail;
    }

    public function moveOneStepByDir(string $direction): void
    {
        $dir = match ($direction) {
            'U' => [0, 1],
            'D' => [0, -1],
            'L' => [-1, 0],
            'R' => [1, 0],
        };
        $this->moveOneStep($dir[0], $dir[1]);
    }

    public function moveOneStep(int $x, int $y): void
    {

        $head = $this->getHead();
        $tail = $this->getTail();

        $head->addX($x);
        $head->addY($y);
        $tailMovedX = 0;
        $tailMovedY = 0;

        if ($head->equalsY($tail) && $head->getAbsDiffX($tail) > 1) {
            $tail->addX($x);
            $tailMovedX = $x;
        }

        if ($head->equalsX($tail) && $head->getAbsDiffY($tail) > 1) {
            $tail->addY($y);
            $tailMovedY = $y;
        }

        if ($head->getAbsDiffX($tail) > 1 && $head->getAbsDiffY($tail) > 1) {
            $tail->addX($x);
            $tail->addY($y);
            $tailMovedX = $x;
            $tailMovedY = $y;
        }

        if ($head->getAbsDiffX($tail) > 1) {
            $tailMovedY = $head->getY() > $tail->getY() ? 1 : -1;
            $tail->addX($x);
            $tail->addY($tailMovedY);
            $tailMovedX = $x;
        }

        if ($head->getAbsDiffY($tail) > 1) {
            $tailMovedX = $head->getX() > $tail->getX() ? 1 : -1;
            $tail->addY($y);
            $tail->addX($tailMovedX);
            $tailMovedY = $y;
        }

        $this->posVisited['' . $tail->getX() . '#' . $tail->getY()] = 1;
        $this->minX = min($this->minX, $tail->getX());
        $this->maxX = max($this->maxX, $tail->getX());
        $this->minY = min($this->minY, $tail->getY());
        $this->maxY = max($this->maxY, $tail->getY());
        if ($tailMovedX !== 0 || $tailMovedY !== 0) {
          //  echo ($this->level === 1) ? 'H' : $this->level;
           // echo ' is now at: ' . $head->getX() . ' ' . $head->getY() . PHP_EOL;
            $this->child?->moveOneStep($tailMovedX, $tailMovedY);
        }
    }

    public function getPosVisited(): array
    {
        return $this->posVisited;
    }

    public function getDeepestCild(): self
    {
        return $this->child ? $this->child->getDeepestCild() : $this;
    }

    public function printVisited(): void
    {
        $posVisited = $this->getPosVisited();
        for ($y = $this->maxY + 1; $y >= $this->minY; $y--) {
            for ($x = $this->minX; $x <= $this->maxX + 1; $x++) {
                if ($x === 0 && $y === 0) {
                    echo 's';
                } else {
                    echo(array_key_exists($x . '#' . $y, $posVisited) ? '#' : '.');
                }
            }
            echo PHP_EOL;
        }
    }
}
