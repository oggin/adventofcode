<?php

namespace AdventOfCode\Riddles\_2022\Components;

class Snafu
{
    private static array $intToSnafu = [
        2 => 2,
        1 => 1,
        0 => 0,
        -1 => "-",
        -2 => "=",
    ];

    private static array $snafuToInt = [
        2 => 2,
        1 => 1,
        0 => 0,
        "-" => -1,
        "=" => -2,
    ];

    public static function fromIntegerValue(int $intVal): string
    {

        $res = [];
        $numOfDigits = self::getNumOfDigits($intVal);
        $rest = $intVal;
        for ($currentDigit = $numOfDigits; $currentDigit > 0; $currentDigit--) {
            $currentPow = pow(5, $currentDigit - 1);
            $multiplier = (int)round($rest / $currentPow);
            $rest = $rest - ($multiplier * $currentPow);
            $res[] = self::intToSnafu($multiplier);
        }
        return implode('', $res);
    }



    private static function intToSnafu(int $int)
    {
        return self::$intToSnafu[$int] ?? $int;
    }

    private static function snafuToInt(string $snafu)
    {
        return self::$snafuToInt[$snafu] ?? $snafu;
    }

    public static function fromSnafuString($snafu): int
    {

        $digits = str_split($snafu);
        $lastDigit = array_pop($digits);
        $convertedNumber = self::snafuToInt($lastDigit);
        $pot = 5;
        while (($lastDigit = array_pop($digits)) !== null) {
            $convertedNumber += $pot * self::snafuToInt($lastDigit);
            $pot *= 5;
        }
        return $convertedNumber;
    }

    public static function getNumOfDigits(int $dec): int
    {
        $minBound = 0;
        $maxBound = 2;
        for ($numOfDigits = 1; $numOfDigits < PHP_INT_MAX; $numOfDigits++) {
            if ($minBound <= $dec && $maxBound >= $dec) {
                return $numOfDigits;
            }
            $minBound = $maxBound + 1;
            $maxBound = (pow(5, $numOfDigits) * 2) + $minBound - 1;
        }

        return $numOfDigits;
    }
}
