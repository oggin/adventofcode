<?php

namespace AdventOfCode\Riddles\_2022\Components\Day5;

class Stack
{
    private array $entries = [];
    private bool $preserveOrderOnMove;

    public function __construct(bool $preserveOrderOnMove)
    {
        $this->preserveOrderOnMove = $preserveOrderOnMove;
    }

    public function moveEntriesToOtherStack(int $numOfEntries, Stack $target)
    {
        $removeEntries = $this->removeEntries($numOfEntries);
        if (!$this->preserveOrderOnMove) {
            $removeEntries = array_reverse($removeEntries);
        }
        $target->addEntries(... $removeEntries);
    }

    public function addEntries(string ...$entrys)
    {
        foreach ($entrys as $entry) {
            $this->entries[] = $entry;
        }
    }

    public function addEntryAtBottom(string $entry)
    {

        array_unshift($this->entries, trim($entry));
    }

    public function removeEntries(int $numOfEntries): array
    {
        return array_splice(
            $this->entries,
            $numOfEntries * -1
        );
    }

    public function getEntryAtTop(): array|string
    {
        return str_replace(['[', ']'], '', (array_slice($this->entries, -1)[0]) ?? ' ');
    }
}
