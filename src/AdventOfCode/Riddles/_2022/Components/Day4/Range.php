<?php

namespace AdventOfCode\Riddles\_2022\Components\Day4;

class Range
{
    private int $startIdx;
    private int $endIdx;
    private Range $secondRange;

    public function __construct(
        int $startIdx,
        int $endIdx,
        int $startIdxSecond,
        int $endIdxSecond,
        bool $isFirst = true
    ) {
        $this->startIdx = $startIdx;
        $this->endIdx = $endIdx;
        if ($isFirst) {
            $this->secondRange = new self($startIdxSecond, $endIdxSecond, $startIdx, $endIdx, false);
        }
    }

    public function overlapsComplete(): bool
    {
        $sIdx1 = $this->startIdx;
        $eIdx1 = $this->endIdx;
        $sIdx2 = $this->secondRange->startIdx;
        $eIdx2 = $this->secondRange->endIdx;
        return $sIdx1 <= $sIdx2 && $eIdx1 >= $eIdx2 || $sIdx2 <= $sIdx1 && $eIdx2 >= $eIdx1;
    }

    public function hasOverlapInAtLeastOnePoint(): bool
    {
        $start = min($this->startIdx, $this->secondRange->startIdx);
        $stop = max($this->endIdx, $this->secondRange->endIdx);
        foreach (range($start, $stop) as $idx) {
            if ($this->isIdxInRange($idx) && $this->secondRange->isIdxInRange($idx)) {
                return true;
            }
        }
        return false;
    }

    public function isIdxInRange(int $idx): bool
    {
        return $this->startIdx <= $idx && $this->endIdx >= $idx;
    }

    public static function fromString(string $range1, string $range2): self
    {
        $split1 = explode('-', $range1);
        $split2 = explode('-', $range2);
        return new self((int)$split1[0], (int)$split1[1], (int)$split2[0], (int)$split2[1]);
    }

    public static function rangesFromString(string $line): Range
    {
        return Range::fromString(... explode(',', $line));
    }
}
