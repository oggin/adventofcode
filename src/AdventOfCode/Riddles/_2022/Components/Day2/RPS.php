<?php

namespace AdventOfCode\Riddles\_2022\Components\Day2;

use RuntimeException;

class RPS
{
    private Hand $yourHand;
    private Hand $opponentsHand;
    private int $pointsByCompareForWin = 6;
    private int $pointsByCompareForDraw = 3;
    private int $pointsByCompareForLoose = 0;

    private int $pointsByFigureForRock = 1;
    private int $pointsByFigureForPaper = 2;
    private int $pointsByFigureForScissors = 3;


    protected function __construct(Hand $opponentsHand, Hand $yourHand)
    {
        $this->yourHand = $yourHand;
        $this->opponentsHand = $opponentsHand;
    }

    public static function fromTwoHands(string $opponent, string $you): RPS
    {
        $yourHand = match ($you) {
            'X' => Hand::ROCK,
            'Y' => Hand::PAPER,
            'Z' => Hand::SCISSORS,
            default => throw new RuntimeException(
                'only letters X,Y or Z (case sensitive) are possible for your hand'
            )
        };

        return new self(self::getOppennentsHand($opponent), $yourHand);
    }

    private static function getOppennentsHand(string $opponent): Hand
    {
        return match ($opponent) {
            'A' => Hand::ROCK,
            'B' => Hand::PAPER,
            'C' => Hand::SCISSORS,
            default => throw new RuntimeException(
                'only letters A B or C (case sensitive) are possible for your opponents hand'
            )
        };
    }

    public function getPointsForCompare(): int
    {
        return match ($this->yourHand->compare($this->opponentsHand)) {
            RpsResult::WIN => $this->pointsByCompareForWin,
            RpsResult::DRAW => $this->pointsByCompareForDraw,
            default => $this->pointsByCompareForLoose,
        };
    }

    public function getPointsForChoosenHand(): int
    {
        return match ($this->yourHand) {
            Hand::SCISSORS => $this->pointsByFigureForScissors,
            Hand::PAPER => $this->pointsByFigureForPaper,
            default => $this->pointsByFigureForRock
        };
    }

    public static function fromOneHandAndResult(string $opponent, string $result): RPS
    {
        $opponentsHand = self::getOppennentsHand($opponent);

        $expectedResultForHandTwo = match ($result) {
            'X' => RpsResult::LOOSE,
            'Y' => RpsResult::DRAW,
            'Z' => RpsResult::WIN,
            default => throw new RuntimeException(
                'only letters X, Y and Z (case sensitive) are possible for expected result'
            )
        };

        return match ($expectedResultForHandTwo) {
            RpsResult::DRAW => new self($opponentsHand, $opponentsHand),
            RpsResult::WIN => new self($opponentsHand, $opponentsHand->getHandToLooseAgainst()),
            default => new self($opponentsHand, $opponentsHand->getHandToWinAgainst())
        };
    }
}
