<?php

namespace AdventOfCode\Riddles\_2022\Components\Day2;

enum RpsResult
{
    case WIN ;
    case DRAW;
    case LOOSE;
}
