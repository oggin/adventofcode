<?php

namespace AdventOfCode\Riddles\_2022\Components\Day2;

enum Hand
{
    case ROCK;
    case PAPER;
    case SCISSORS;

    /**
     * compare your hand with other hand
     * @param Hand $otherHand
     * @return RpsResult result for your hand
     */
    public function compare(Hand $otherHand): RpsResult
    {
        return match ($this) {
            $otherHand => RpsResult::DRAW,
            $otherHand->getHandToLooseAgainst() => RpsResult::WIN,
            default => RpsResult::LOOSE
        };
    }

    public function getHandToLooseAgainst(): Hand
    {
        return match ($this) {
            Hand::ROCK => Hand::PAPER,
            Hand::SCISSORS => Hand::ROCK,
            default => Hand::SCISSORS
        };
    }

    public function getHandToWinAgainst(): Hand
    {
        return match ($this) {
            Hand::ROCK => Hand::SCISSORS,
            Hand::SCISSORS => Hand::PAPER,
            default => Hand::ROCK
        };
    }
}
