<?php

namespace AdventOfCode\Riddles\_2022\Components\Day7;

class File
{
    private int $size;

    public function __construct(int $size)
    {
        $this->size = $size;
    }

    public static function fromString(string $input): ?self
    {
        if ((int)preg_match('~^(?<size>\d+)\s(?<name>.*)$~', $input, $match) === 1) {
            return new self($match['size']);
        }
        return null;
    }

    public function getSize(): int
    {
        return $this->size;
    }
}
