<?php

namespace AdventOfCode\Riddles\_2022\Components\Day7;

use AdventOfCode\Exception\AdventOfCodeException;

class Command
{
    private string $cmd;

    public function __construct(string $command)
    {
        $this->cmd = substr($command, 2);
    }

    public static function fromString(string $input): ?self
    {
        return str_starts_with($input, '$') ? new self($input) : null;
    }

    public function isLs(): bool
    {
        return $this->cmd === 'ls';
    }

    public function isCdUp(): bool
    {
        return $this->cmd === 'cd ..';
    }

    public function isSubdir(): bool
    {
        return (bool)preg_match('~cd\s[A-z0-9]+~', $this->cmd);
    }

    public function isCdRoot(): bool
    {
        return $this->cmd === 'cd /';
    }

    /**
     * @throws AdventOfCodeException
     */
    public function getSubdir(): string
    {
        if (!$this->isSubdir()) {
            throw new AdventOfCodeException('command has no subdir');
        }

        return str_replace('cd ', '', $this->cmd);
    }

    public function getCmd(): string
    {
        return $this->cmd;
    }
}
