<?php

namespace AdventOfCode\Riddles\_2022\Components\Day7;

use AdventOfCode\Exception\AdventOfCodeException;

class Dir
{
    private string $dirname;
    /**
     * @var array<int, Dir>
     */
    private array $subDirs = [];

    private ?Dir $parent = null;
    private int $size = 0;

    private function __construct(string $name)
    {
        $this->dirname = $name;
    }

    public static function asRootDir(): Dir
    {
        return new Dir('/');
    }

    public function fromString(string $input): ?self
    {
        $dir = new self(str_replace('dir ', '', $input));
        $dir->parent = $this;
        $this->subDirs[$dir->dirname] = $dir;
        return $dir;
    }


    /**
     * @throws AdventOfCodeException
     */
    public function getSubdir(string $dirname): Dir
    {
        return $this->subDirs[$dirname] ?? throw new AdventOfCodeException('not exists ' . $dirname . ' in folder ' . $this->dirname);
    }

    public function isRoot(): bool
    {
        return $this->dirname === '/';
    }

    public function goToRoot(): Dir
    {
        if ($this->isRoot()) {
            return $this;
        }
        return $this->parent->goToRoot();
    }

    /**
     * @throws AdventOfCodeException
     */
    public function executeCommand(Command $command): Dir
    {
        //can be ignored
        if ($command->isLs()) {
            return $this;
        }
        if ($command->isCdUp()) {
            if ($this->isRoot()) {
                throw new AdventOfCodeException('cant go up on root');
            }
            return $this->parent;
        }

        if ($command->isCdRoot()) {
            return $this->goToRoot();
        }

        if ($command->isSubdir()) {
            return $this->getSubdir($command->getSubdir());
        }

        throw new AdventOfCodeException('unknown command' . $command->getCmd());
    }

    public function addFile(File $file): void
    {
        $this->updateSize($file);
    }

    private function updateSize(File $file): void
    {
        $this->size += $file->getSize();
        $this->parent?->updateSize($file);
    }


    public static function isDir(string $name): bool
    {
        return str_starts_with($name, 'dir ');
    }

    public function getSizeStack(&$stack = []): int
    {
        foreach ($this->subDirs as $dir) {
            $dir->getSizeStack($stack);
        }
        $stack[] = $this->size;
        return $this->size;
    }

    public function getTotalFileSize(int $minSize, &$sum = 0): int
    {
        foreach ($this->subDirs as $dir) {
            if (($size = $dir->getTotalFileSize($minSize, $sum)) <= $minSize) {
                $sum += $size;
            }
        }
        return $this->size <= $minSize ? $this->size : 0;
    }

    public function getSize(): int
    {
        return $this->size;
    }
}
