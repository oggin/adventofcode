<?php

namespace AdventOfCode\Riddles\_2022\Components\Day15;

class Point
{
    private int $x;
    private int $y;
    private bool $isSensor;
    private bool $isBeacon;
    private ?bool $canBeBeacon = null;
    private ?Point $beacon = null;

    /**
     * @param int $x
     * @param int $y
     * @param bool $isSensor
     * @param bool $isBeacon
     */
    public function __construct(int $x, int $y, bool $isSensor = false, bool $isBeacon = false)
    {
        $this->x = $x;
        $this->y = $y;
        $this->isSensor = $isSensor;
        $this->isBeacon = $isBeacon;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function setIsSensor(bool $isSensor): void
    {
        $this->isSensor = $isSensor;
    }

    public function setIsBeacon(bool $isBeacon): void
    {
        $this->isBeacon = $isBeacon;
    }

    public function setBeacon(?Point $beacon): void
    {
        $this->beacon = $beacon;
    }

    public function getManhattanDistance(): int
    {
        if (!$this->beacon) {
            return 0;
        }
        return abs($this->x - $this->beacon->x) + abs($this->y - $this->beacon->y);
    }

    public function getCanBeBeacon(): ?bool
    {
        return $this->canBeBeacon;
    }

    public function setCanBeBeacon(?bool $canBeBeacon): void
    {
        if (!$this->isBeacon && !$this->isSensor()) {
            $this->canBeBeacon = $canBeBeacon;
        }
    }

    public function isSensor(): bool
    {
        return $this->isSensor;
    }

    public function isBeacon(): bool
    {
        return $this->isBeacon;
    }
}
