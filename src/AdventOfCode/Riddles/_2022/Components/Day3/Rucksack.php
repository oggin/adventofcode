<?php

namespace AdventOfCode\Riddles\_2022\Components\Day3;

use AdventOfCode\Exception\AdventOfCodeException;

use function str_contains;

class Rucksack
{
    private string $content;
    private const DIFF_ORD_LOWERCASE = 96;
    private const DIFF_ORD_UPPERCASE = 38;
    private const ORD_UPPERCASE_A = 65;
    private const ORD_UPPERCASE_Z = 90;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    private function isUppercase(int $ord): bool
    {
        return $ord > self::ORD_UPPERCASE_A && $ord <= self::ORD_UPPERCASE_Z;
    }

    public function charToInt($char): int
    {
        return ($ord = ord($char)) - ($this->isUppercase($ord) ? self::DIFF_ORD_UPPERCASE : self::DIFF_ORD_LOWERCASE);
    }

    /**
     * @throws AdventOfCodeException
     */
    public function getCommonItemsInRucksacks(Rucksack ...$rucksacks): int
    {
        $split1 = str_split($this->content);

        foreach ($split1 as $char) {
            if ($this->checkAllRucksacksForChar($char, ...$rucksacks) === true) {
                return $this->charToInt($char);
            }
        }
        throw new AdventOfCodeException('no common char found');
    }

    private function checkAllRucksacksForChar(string $char, Rucksack ...$rucksacks): bool
    {
        foreach ($rucksacks as $rucksack) {
            if (!str_contains($rucksack->content, $char)) {
                return false;
            }
        }
        return true;
    }
}
