<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022\Components\Day11;

use AdventOfCode\Classes\Helper;

class Monkey
{
    private int $throwToMonkeyOnTrue;
    private int $throwToMonkeyOnFalse;
    private int $testDivisibleBy;

    private int $numOfItemsInspected = 0;

    /**
     * @var array<int>
     */
    private array $items;
    /**
     * @var string A = addition, M = multiply
     */
    private string $operation;
    /**
     * @var int|null   value to add/multiply or null to use self
     */
    private ?int $operator;
    private int $divideWorryLevelBy = 3;
    private int $superMod = 0;


    public function __construct(array $data)
    {

        foreach (Helper::castElementsToInt(explode(', ', str_replace('Starting items: ', '', $data[1]))) as $item) {
            $this->addItem($item);
        }
        $operations = explode(' ', trim(str_replace('Operation: new = old ', '', $data[2])));
        $this->operation = trim($operations[0]) === '*' ? 'M' : 'A';
        $this->operator = trim($operations[1]) === 'old' ? null : (int)$operations[1];
        $this->testDivisibleBy = (int)str_replace('Test: divisible by ', '', $data[3]);
        $this->throwToMonkeyOnTrue = (int)str_replace('If true: throw to monkey ', '', $data[4]);
        $this->throwToMonkeyOnFalse = (int)str_replace('If false: throw to monkey ', '', $data[5]);
    }

    /**
     * @param Monkey[] $monkeys
     * @return void
     */

    public function inspectItems(array $monkeys): void
    {

        while (($item = array_shift($this->items)) !== null) {
            $this->numOfItemsInspected++;
            $itemUpdated = $this->updateWorry($item);
            $targetMonkey = $this->getTargetMonkey($itemUpdated);
            $monkeys[$targetMonkey]->addItem($itemUpdated);
        }
    }

    private function updateWorry(int $item): int
    {
        $secondParam = $this->operator ?? $item;
        $updatedValue = $this->operation === 'A' ? $item + $secondParam : $item * $secondParam;

        if ($this->divideWorryLevelBy > 1) {
             return (int)($updatedValue / $this->divideWorryLevelBy);
        }

        return $updatedValue % $this->superMod;
    }

    public function addItem(int $item): void
    {
        $this->items[] = $item;
    }

    public function getNumOfItemsInspected(): int
    {
        return $this->numOfItemsInspected;
    }

    /**
     * @param Monkey[] $monkeyStack
     * @return int
     */
    public function getMostBusyMultiply(array $monkeyStack): int
    {
        $counts = [];
        foreach ($monkeyStack as $monkey) {
            $counts[] = $monkey->getNumOfItemsInspected();
        }
        asort($counts);
        return array_pop($counts) * array_pop($counts);
    }

    public function setDivideWorryLevelBy(int $divideWorryLevelBy): void
    {
        $this->divideWorryLevelBy = $divideWorryLevelBy;
    }

    /**
     * @param Monkey[] $monkeys
     */

    public function setSuperMod(array $monkeys): void
    {
        $superMod = 1;
        foreach ($monkeys as $monkey) {
            $superMod *= $monkey->testDivisibleBy;
        }
        $this->superMod = $superMod;
    }

    private function getTargetMonkey(int $itemUpdated): int
    {
        $targetMonkey = (($itemUpdated % $this->testDivisibleBy) === 0) ? $this->throwToMonkeyOnTrue : $this->throwToMonkeyOnFalse;
        return $targetMonkey;
    }
}
