<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2022\Components\Day5\Stack;

class Day5 extends MainRiddle
{
    public static int $day = 5;
    public static int $year = 2022;
    private bool $readCmd = false;
    public static bool $trimInput = false;
    public bool $isCrateMover9001 = false;

    /**
     * @var array<int,Stack>
     */
    private array $stacks = [];

    public function calcResultString(): string
    {
        foreach ($this->lines as $line) {
            $this->handleSingleLine($line);
        }

        return $this->getFinalResult();
    }
    public function calcResult2String(): string
    {
        $this->isCrateMover9001 = true;
        foreach ($this->lines as $line) {
            $this->handleSingleLine($line);
        }

        return $this->getFinalResult();
    }

    public function calcResult2(): int
    {
        return 5 * 5;
    }

    private function readStacks(mixed $line): void
    {
        $lineData = str_split($line, 4);
        foreach ($lineData as $colIdx => $entry) {
            if (trim($entry) !== '') {
                $this->stacks[$colIdx] ??= new Stack($this->isCrateMover9001);
                $this->stacks[$colIdx]->addEntryAtBottom($entry);
            }
        }
    }

    private function isNumberLine(mixed $line): bool
    {
        return (bool)preg_match('~^[\d\s]+$~', $line);
    }

    public function calcResult(): int
    {
        return -1;
    }

    private function getFinalResult(): string
    {
        $result = [];
        for ($idx = 0; $idx < count($this->stacks); $idx++) {
            $result[] = $this->stacks[$idx]->getEntryAtTop();
        }
        return implode('', $result);
    }

    private function moveEntries(string $line): void
    {
        preg_match('~move\s(?<amount>\d+)\sfrom\s(?<from>\d+)\sto\s(?<to>\d+)~', $line, $match);

        $this->stacks[(int)$match['from'] - 1]->moveEntriesToOtherStack(
            (int)$match['amount'],
            $this->stacks[(int)$match['to'] - 1]
        );
    }

    private function handleSingleLine(mixed $line): void
    {
        if ($this->isNumberLine($line) || $line === '') {
            $this->readCmd = true;
            return;
        }

        if (!$this->readCmd) {
            $this->readStacks($line);
        } else {
            $this->moveEntries($line);
        }
    }
}
