<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;

class Day1 extends MainRiddle
{
    public static int $day = 1;
    public static int $year = 2022;
    private array $stack = [];

    public function calcResult(): int
    {
        $this->buildStack();
        return max(($this->stack));
    }

    public function calcResult2(): int
    {
        $this->buildStack();
        sort($this->stack);
        return array_sum(array_slice($this->stack, -3));
    }

    private function buildStack(): void
    {
        $sumByElv = 0;
        $this->stack = [];
        $totalLineCount = count($this->lines);
        foreach ($this->lines as $lineNumber => $line) {
            if ($line === '' || $totalLineCount === $lineNumber) {
                $this->stack[] = $sumByElv;
                $sumByElv = 0;
            } else {
                $sumByElv += (int)$line;
            }
        }
    }
}
