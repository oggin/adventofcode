<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2022\Components\Day3\Rucksack;

use function count;

class Day3 extends MainRiddle
{
    public static int $day = 3;
    public static int $year = 2022;
    private int $sum = 0;

    public function calcResult(): int
    {
        foreach ($this->lines as $line) {
            $split = str_split($line, strlen($line) / 2);
            $this->sum += (new Rucksack($split[0]))->getCommonItemsInRucksacks(new Rucksack($split[1]));
        }
        return $this->sum;
    }

    public function calcResult2(): int
    {
        for ($idx = 0; $idx < count($this->lines) - 2; $idx += 3) {
            $rucksack = new Rucksack($this->lines[$idx]);
            $this->sum += $rucksack->getCommonItemsInRucksacks(new Rucksack($this->lines[$idx + 1]), new Rucksack($this->lines[$idx + 2]));
        }
        return $this->sum;
    }
}
