<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2022\Components\Day15\Point;

class Day15 extends MainRiddle
{
    public static int $day = 15;
    public static int $year = 2022;
    /**
     * @var array<int,array<int,Point>>
     */
    private array $grid;

    public function calcResult(): int
    {

        //$theOldWay = $this->prepareOutputForDemoData();
        return $this->countByRow(2000000);
    }

    private function markPointsInDistance(Point $point, int $man)
    {
        for ($curY = 0; $curY <= $man; $curY++) {
            for ($curX = 0; $curX <= $man - $curY; $curX++) {
                $this->getPoint($point->gety() + $curY, $point->getX() + $curX)?->setCanBeBeacon(false);
                $this->getPoint($point->getY() + $curY, $point->getX() - $curX)?->setCanBeBeacon(false);
                $this->getPoint($point->getY() - $curY, $point->getX() + $curX)?->setCanBeBeacon(false);
                $this->getPoint($point->getY() - $curY, $point->getX() - $curX)?->setCanBeBeacon(false);
            }
        }
    }

    public function calcResult2(): int
    {
        return 15 * 15;
    }

    public function getPoint(int $y, int $x): ?Point
    {
        return $this->grid[$y][$x] ?? null;
    }

    public function draw(): array
    {
        $countByY = [];
        echo "\t";
        foreach (array_keys(reset($this->grid)) as $idx) {
            echo $idx . "\t";
        }
        echo PHP_EOL;
        foreach ($this->grid as $rowIdxAkaY => $col) {
            echo $rowIdxAkaY . "\t";
            foreach ($col as $point) {
                if ($point->isSensor()) {
                    echo 'S';
                } elseif ($point->isBeacon()) {
                    echo 'B';
                } elseif ($point->getCanBeBeacon() === false) {
                    $countByY[$rowIdxAkaY] = ($countByY[$rowIdxAkaY] ?? 0) + 1;
                    echo '.';
                } else {
                    echo ' ';
                }
                echo "\t";
            }
            echo "\t-$rowIdxAkaY-" . ($countByY[$rowIdxAkaY] ?? 0) . "-" . PHP_EOL;
        }
        ksort($countByY);
        return $countByY;
    }

    private function prepareOutputForDemoData(): array
    {
        /**
         * @var array<int,array<int,Point>> $tmpPoints
         */
        $tmpPoints = [];
        $minx = PHP_INT_MAX;
        $minY = PHP_INT_MAX;
        $maxX = PHP_INT_MIN;
        $maxY = PHP_INT_MIN;
        foreach ($this->lines as $line) {
            preg_match('~x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)~', $line, $match);
            $sX = (int)$match[1];
            $sY = (int)$match[2];
            $bX = (int)$match[3];
            $bY = (int)$match[4];
            $beacon = $tmpPoints[$bY][$bX] ?? new Point(x: $bX, y: $bY, isBeacon: true);
            $beacon->setIsBeacon(true);
            $tmpPoints[$bY][$bX] = $beacon;

            $sensor = $tmpPoints[$sY][$sX] ?? new Point(x: $sX, y: $sY, isSensor: true);
            $sensor->setIsSensor(true);
            $sensor->setBeacon($beacon);
            $tmpPoints[$sY][$sX] = $sensor;
            $minx = min($sX, $bX, $minx);
            $minY = min($sY, $bY, $minY);

            $maxX = max($sX, $bX, $maxX);
            $maxY = max($sY, $bY, $maxY);
        }

        $this->grid = array_fill($minY - 10, ($maxY + abs($minY)) + 10, []);

        foreach (array_keys($this->grid) as $idx) {
            $this->grid[$idx] = array_fill($minx - 10, ($maxX + (abs($minx))) + 15, null);
        }


        foreach ($tmpPoints as $y => $points) {
            foreach ($points as $x => $point) {
                $this->grid[$y][$x] = $point;
            }
        }

        foreach ($this->grid as $y => $grid) {
            foreach ($grid as $x => $item) {
                if ($item === null) {
                    $this->grid[$y][$x] = new Point($x, $y);
                }
            }
        }


        foreach ($tmpPoints as $points) {
            foreach ($points as $point) {
                if (($man = $point->getManhattanDistance()) > 0) {
                    $this->markPointsInDistance($point, $man);
                }
            }
        }

        return $this->draw();
    }

    private function countByRow($yValToCheck): ?int
    {
        $tmpPoints = [];
        $coordsOfSensorsAndBeacons = [];
        $poi = [];


        foreach ($this->lines as $line) {
            preg_match('~x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)~', $line, $match);
            $sX = (int)$match[1];
            $sY = (int)$match[2];
            $bX = (int)$match[3];
            $bY = (int)$match[4];
            $beacon = $tmpPoints[$bY][$bX] ?? new Point(x: $bX, y: $bY, isBeacon: true);
            $beacon->setIsBeacon(true);
            $tmpPoints[$bY][$bX] = $beacon;
            $coordsOfSensorsAndBeacons[] = $bY . '#' . $bX;
            $coordsOfSensorsAndBeacons[] = $sY . '#' . $sX;

            $sensor = $tmpPoints[$sY][$sX] ?? new Point(x: $sX, y: $sY, isSensor: true);
            $sensor->setIsSensor(true);
            $sensor->setBeacon($beacon);
            $tmpPoints[$sY][$sX] = $sensor;

            $min = $sensor->getY() - $sensor->getManhattanDistance();
            $max = $sensor->getY() + $sensor->getManhattanDistance();
            if ($min <= $yValToCheck && $max >= $yValToCheck) {
                $poi[] = $sensor;
            }
        }

        $notReachable = [];

        foreach ($poi as $point) {
            $manhattan = $point->getManhattanDistance();
            $absValToCheck = abs($yValToCheck);
            $absCurY = abs($point->getY());
            $diff = abs($manhattan - abs(($absValToCheck - $absCurY)));
            $x = $point->getX();

            foreach (range($x - $diff, $x + $diff) as $idx) {
                if (!in_array($yValToCheck . '#' . $idx, $coordsOfSensorsAndBeacons)) {
                    $notReachable[$idx] = 1;
                }
            }
        }

        return count($notReachable);
    }
}
