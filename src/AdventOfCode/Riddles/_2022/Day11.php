<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2022\Components\Day11\Monkey;

class Day11 extends MainRiddle
{
    public static int $day = 11;
    public static int $year = 2022;
    /**
     * @var Monkey[]
     */
    private array $monkeyStack;

    public function calcResult(): int
    {
        $this->buildMonkeyStack(3);
        return $this->playXRounds(20);
    }

    public function calcResult2(): int
    {
        $this->buildMonkeyStack(1);
        return $this->playXRounds(10000);
    }

    private function buildMonkeyStack(int $divideWorrysBy)
    {
        $data = [];
        $this->monkeyStack = [];
        foreach ($this->lines as $line) {
            if ($line === '') {
                continue;
            }
            $data[] = $line;
            if (str_starts_with(trim($line), 'If false: throw to monkey')) {
                $monkey = new Monkey($data);
                $this->monkeyStack[] = $monkey;
                $data = [];
            }
        }
        foreach ($this->monkeyStack as $monkey) {
            $monkey->setDivideWorryLevelBy($divideWorrysBy);
            $monkey->setSuperMod($this->monkeyStack);
        }
    }

    private function playXRounds(int $roundsToPlay): int
    {
        for ($round = 1; $round <= $roundsToPlay; $round++) {
            foreach ($this->monkeyStack as $monkey) {
                $monkey->inspectItems($this->monkeyStack);
            }
        }

        return $this->monkeyStack[0]->getMostBusyMultiply($this->monkeyStack);
    }
}
