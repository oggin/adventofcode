<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Exception\InputException;
use AdventOfCode\Riddles\_2022\Components\Day2\RPS;

use function count;

class Day2 extends MainRiddle
{
    public static int $day = 2;
    public static int $year = 2022;

    /**
     * @throws InputException
     */
    public function calcResult(): int
    {
        $score = 0;
        foreach ($this->lines as $line) {
            $gameData = $this->getGameDataForLine($line);
            $rps = RPS::fromTwoHands($gameData[0], $gameData[1]);
            $score += $rps->getPointsForCompare() + $rps->getPointsForChoosenHand();
        }
        return $score;
    }

    /**
     * @throws InputException
     */
    public function calcResult2(): int
    {
        $score = 0;
        foreach ($this->lines as $line) {
            $gameData = $this->getGameDataForLine($line);
            $rps = RPS::fromOneHandAndResult($gameData[0], $gameData[1]);
            $score += $rps->getPointsForCompare() + $rps->getPointsForChoosenHand();
        }
        return $score;
    }

    /**
     * @throws InputException
     */
    private function getGameDataForLine(mixed $line): array
    {
        $gameData = explode(' ', $line);
        if (count($gameData) !== 2) {
            throw new InputException('invalid input ' . $line);
        }
        return $gameData;
    }
}
