<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2022\Components\Day4\Range;

class Day4 extends MainRiddle
{
    public static int $day = 4;
    public static int $year = 2022;
    private int $overlapCnt = 0;
    protected ?string $inputPattern = '~^\d+-\d+,\d+-\d+$~';

    public function calcResult(): int
    {
        foreach ($this->lines as $line) {
            $this->overlapCnt += Range::rangesFromString($line)->overlapsComplete() ? 1 : 0;
        }

        return $this->overlapCnt;
    }

    public function calcResult2(): int
    {
        foreach ($this->lines as $line) {
            $this->overlapCnt += Range::rangesFromString($line)->hasOverlapInAtLeastOnePoint() ? 1 : 0;
        }

        return $this->overlapCnt;
    }
}
