<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;

class Day14 extends MainRiddle
{
    public static int $day = 14;
    public static int $year = 2022;
    public static string $rockMarker = '#';
    public static string $emptyMarker = '.';

    public int $maxDeepth = 0;
    public int $sandFallen = 0;
    public bool $saveSimulation = false;

    /**
     * @var array<int,array<int,string>>
     */
    private array $grid = [];

    public function calcResult(): int
    {
        $this->init();
        do {
            $simulte = $this->simulateSandFall() === false;

        } while ($simulte);

        $this->print();
        return $this->sandFallen - 1;
    }

    private function drawLine(int $curRowIdx, int $curColIdx, int $newRowIdx, int $newColIdx)
    {
        if ($curColIdx === $newColIdx) {
            $fromRow = min($curRowIdx, $newRowIdx);
            $toRow = max($curRowIdx, $newRowIdx);
            for ($row = $fromRow; $row <= $toRow; $row++) {
                $this->grid[$row][$curColIdx] = self::$rockMarker;
            }
            return;
        }
        if ($curRowIdx === $newRowIdx) {
            $fromCol = min($curColIdx, $newColIdx);
            $toCol = max($curColIdx, $newColIdx);
            for ($col = $fromCol; $col <= $toCol; $col++) {
                $this->grid[$curRowIdx][$col] = self::$rockMarker;
            }
        }
    }


    public function calcResult2(): int
    {
        $this->init(true);

        do {
            $simulate = $this->simulateSandFall(false) === false;
        } while ($simulate);
        $this->print();
        return $this->sandFallen;
    }

    private function print(): void
    {
        if ($this->saveSimulation === false) {
            return;
        }
        $out = '';
        foreach ($this->grid as $rowIdx => $row) {
            if ($rowIdx === 0) {
                continue;
            }
            foreach ($row as $val) {
                $out .= $val . "\t";
            }
            $out .= PHP_EOL;
        }

        for ($col = 0; $col < count($this->grid[1]); $col++) {
            $out .= $col . "\t";
        }


        //echo $out;
        file_put_contents('cave.csv', $out);
    }


    private function simulateSandFall(bool $stopOnBottom = true): bool
    {
        $curRow = 0;
        $curCol = 500;

        do {
            $elementBelow = $this->grid[$curRow + 1][$curCol];
            if ($elementBelow === self::$emptyMarker) {
                $curRow++;
            } elseif ($elementBelow == self::$rockMarker || (int)$elementBelow > 0) {
                if ($this->grid[$curRow + 1][$curCol - 1] === self::$emptyMarker) {
                    $curRow += 1;
                    $curCol -= 1;
                } elseif ($this->grid[$curRow + 1][$curCol + 1] === self::$emptyMarker) {
                    $curRow += 1;
                    $curCol += 1;
                } else {
                    break;
                }
            } else {
                break;
            }
        } while ($curRow + 2 < $this->maxDeepth);
        $this->grid[$curRow][$curCol] = ++$this->sandFallen;
        if ($stopOnBottom) {
            return $curRow + 2 === $this->maxDeepth && $this->grid[$curRow + 1][$curCol] === self::$emptyMarker;
        }
        return $curCol === 500 && $curRow === 0;
    }

    private function getMaxRowAndColCnt(): array
    {
        preg_match_all('~^(\d+)| \d+~', $this->content, $matchRows);
        preg_match_all('~,(\d+)~', $this->content, $matchCols);


        return ['rows' => max(array_unique($matchRows[0])), 'cols' => max(array_unique($matchCols[1]))];
    }

    private function init(bool $hasBottom = false): void
    {
        $max = $this->getMaxRowAndColCnt();

        $this->maxDeepth = (int)$max['cols'] + ($hasBottom ? +3 : 1);
        $this->grid = array_fill(0, $this->maxDeepth, []);
        foreach ($this->grid as $idx => $r) {
            $this->grid[$idx] = array_fill(0, ((int)$max['rows'] + 1) * 2, '.');
        }
        foreach ($this->lines as $line) {
            //513,151 -> 513,155 -> 510,155 -> 510,161 -> 519,161 -> 519,155 -> 515,155 -> 515,151
            $coords = explode(' -> ', $line);
            $curColIdx = $curRowIdx = null;
            foreach ($coords as $point) {
                [$newColIdx, $newRowIdx] = explode(',', $point);
                $newColIdx = (int)$newColIdx;
                $newRowIdx = (int)$newRowIdx;
                if ($curColIdx !== null) {
                    $this->drawLine($curRowIdx, $curColIdx, $newRowIdx, $newColIdx);
                }

                $curColIdx = $newColIdx;
                $curRowIdx = $newRowIdx;
            }
        }
    }
}
