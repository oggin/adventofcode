<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Exception\AdventOfCodeException;
use AdventOfCode\Riddles\_2022\Components\Day10\Cpu;

class Day10 extends MainRiddle
{
    public static int $day = 10;
    public static int $year = 2022;


    public function calcResult(): int
    {
        return $this->getCpu()->getCycleXProduct();
    }

    public function calcResult2String(): string
    {
        return $this->getCpu()->printCRT();
    }

    private function getCpu(): Cpu
    {
        $command = [];
        foreach ($this->lines as $line) {
            $part = explode(' ', $line);
            $command[] = [$part[0], (count($part) > 1 ? (int)$part[1] : '-')];
        }

        return new Cpu($command);
    }


    public function calcResult2(): int
    {
        throw new AdventOfCodeException('this riddle returns string');
    }
}
