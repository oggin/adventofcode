<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Exception\AdventOfCodeException;
use AdventOfCode\Riddles\_2022\Components\Day7\Command;
use AdventOfCode\Riddles\_2022\Components\Day7\Dir;
use AdventOfCode\Riddles\_2022\Components\Day7\File;

class Day7 extends MainRiddle
{
    public static int $day = 7;
    public static int $year = 2022;
    private ?Dir $rootDir = null;

    public function calcResult(): int
    {
        $this->initTree();
        $this->rootDir->getTotalFileSize(100000, $sum);
        return $sum;
    }

    public function calcResult2(): int
    {
        $this->initTree();

        $neededSpaceToFree = 30000000 - 70000000 + $this->rootDir->getSize();
        $this->rootDir->getSizeStack($stack);
        sort($stack);
        foreach ($stack as $size) {
            if ($size >= $neededSpaceToFree) {
                return $size;
            }
        }
        return 0;
    }

    /**
     * @throws AdventOfCodeException
     */
    private function initTree(): void
    {
        $this->rootDir = Dir::asRootDir();
        $currentDir = $this->rootDir;
        foreach ($this->lines as $line) {
            if (($cmd = Command::fromString($line)) !== null) {
                $currentDir = $currentDir->executeCommand($cmd);
            } elseif (($file = File::fromString($line)) !== null) {
                $currentDir->addFile($file);
            } elseif (Dir::isDir($line)) {
                $currentDir->fromString($line);
            } else {
                throw new AdventOfCodeException('no match ' . $line);
            }
        }
    }
}
