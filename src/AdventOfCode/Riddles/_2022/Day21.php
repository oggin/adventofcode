<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2022\Components\Day21\Monkey;
use AdventOfCode\Riddles\_2022\Components\Day21\MonkeyStack;
use Exception;

class Day21 extends MainRiddle
{
    public static int $day = 21;
    public static int $year = 2022;

    /**
     * @throws Exception
     */
    public function calcResult(): int
    {

        return (new MonkeyStack($this->lines))->getMonkey('root')->getResult();


    }

    /**
     * @throws Exception
     */
    public function calcResult2(): int
    {
        $monkeyStack = new MonkeyStack($this->lines);
        $humMonkey = $monkeyStack->getMonkey('humn');
        $rootMonkey = $monkeyStack->getMonkey('root');
        $rootMonkey->calcBack($humMonkey);

        return $humMonkey->getResult();
    }

}
