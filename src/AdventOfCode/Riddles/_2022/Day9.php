<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2022\Components\Day9\Node;

class Day9 extends MainRiddle
{
    public static int $day = 9;
    public static int $year = 2022;
    private bool $debug = false;

    public function calcResult(): int
    {
        return $this->new();
    }


    public function calcResult2(): int
    {
        return $this->new(9);
    }

    private function new(int $nodes = 1): ?int
    {
        $node = new Node($nodes);

        foreach ($this->lines as $line) {
            [$direction, $steps] = explode(' ', $line);
            //echo "####### Going $steps step(s) $direction #######" . PHP_EOL;
            for ($step = 0; $step < $steps; $step++) {
                $node->moveOneStepByDir($direction);
                //  echo PHP_EOL;
            }

        }

        $node1 = $node->getDeepestCild();
        if ($this->debug) {
            $node1->printVisited();
        }

        return count($node1->getPosVisited());
    }
}
