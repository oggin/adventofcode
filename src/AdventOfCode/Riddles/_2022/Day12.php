<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;

class Day12 extends MainRiddle
{
    public static int $day = 12;
    public static int $year = 2022;

    public function calcResult(): int
    {
        return 3;
    }

    public function calcResult2(): int
    {
        return 12 * 12;
    }
}
