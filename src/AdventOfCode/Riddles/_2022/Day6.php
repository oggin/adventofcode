<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;

class Day6 extends MainRiddle
{
    public static int $day = 6;
    public static int $year = 2022;

    public function calcResult(): int
    {
        return $this->getPostForFirstDiffChunk(4);
    }

    public function calcResult2(): int
    {
        return $this->getPostForFirstDiffChunk(14);
    }

    private function getPostForFirstDiffChunk(int $chunkSize): int
    {
        $split = str_split($this->lines[0]);
        for ($i = $chunkSize + 1; $i < count($split); $i++) {
            $chunk = array_slice($split, $i - $chunkSize, $chunkSize);
            if (count(array_count_values($chunk)) === $chunkSize) {
                return $i;
            }
        }

        return 0;
    }
}
