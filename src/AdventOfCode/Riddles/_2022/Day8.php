<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2022\Components\Day8\TreeHouseGrid;

class Day8 extends MainRiddle
{
    public static int $day = 8;
    public static int $year = 2022;
    /**
     * @var array<int,TreeHouseGrid>
     */
    private array $colstack = [];


    private ?TreeHouseGrid $grid = null;

    public function calcResult(): int
    {
        $this->buildGrid();

        return $this->grid->getVisibleFromOutsideCount();
    }

    public function calcResult2(): int
    {
        $this->buildGrid();

        return $this->grid->getHighestScenicScore();
    }

    private function buildGrid(): void
    {
        $this->grid = new TreeHouseGrid(count($this->lines));
        foreach ($this->lines as $rowIdx => $line) {
            foreach (str_split($line) as $colIdx => $number) {
                $this->grid->addElement($rowIdx, $colIdx, (int)$number);
            }
        }
    }
}
