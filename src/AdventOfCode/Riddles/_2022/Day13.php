<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;

class Day13 extends MainRiddle
{
    public static int $day = 13;
    public static int $year = 2022;

    public function calcResult(): int
    {
        $correctOrderByIndex = [];
        $mainIndex = 1;
        for ($idx = 0; $idx < count($this->lines) - 1; $idx += 3, $mainIndex++) {
            $left = json_decode($this->lines[$idx]);
            $right = json_decode($this->lines[$idx + 1]);
            $correctOrderByIndex[$mainIndex] = $this->compare($left, $right);
        }
        $sum = 0;
        foreach ($correctOrderByIndex as $idx => $val) {
            if ($val > 0) {
                $sum += $idx;
            }
        }

        return $sum;
    }

    public function calcResult2(): int
    {
        $data = [];
        for ($idx = 0; $idx < count($this->lines) - 1; $idx += 3) {
            $data[] = json_decode($this->lines[$idx]);
            $data[] = json_decode($this->lines[$idx + 1]);
        }

        $data[] = [[2]];
        $data[] = [[6]];
        $div2 = $div6 = 1;
        foreach ($data as $d) {
            if ($this->compare($d, [[2]]) == 1) {
                $div2++;
            }
            if ($this->compare($d, [[6]]) == 1) {
                $div6++;
            }
        }

        return $div2 * $div6;
    }

    private function compare($left, $right): int
    {
        if ($left === null) {
            return 1;
        }

        if ($right === null) {
            return -1;
        }

        $leftIsInt = is_int($left);
        $leftIsArray = is_array($left);
        $rightIsInt = is_int($right);
        $rightIsArray = is_array($right);

        if ($leftIsInt && $rightIsInt) {
            return $this->compareInt($left, $right);
        }


        if ($leftIsArray && $rightIsInt) {
            return $this->compare($left, [$right]);
        }

        if ($rightIsArray && $leftIsInt) {
            return $this->compare([$left], $right);
        }

        if ($leftIsArray && $rightIsArray) {
            while (($l = array_shift($left)) !== null) {
                if (($result = $this->compare($l, array_shift($right))) !== 0) {
                    return $result;
                }
            }
            if (count($left) < count($right)) {
                return 1;
            }

            if (count($left) > count($right)) {
                return -1;
            }
        }


        return 0;
    }

    private function compareInt($left, $right): int
    {
        if ($left < $right) {
            return 1;
        }

        if ($left > $right) {
            return -1;
        }
        return 0;
    }
}
