<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2022;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Exception\AdventOfCodeException;
use AdventOfCode\Riddles\_2022\Components\Snafu;

class Day25 extends MainRiddle
{
    public static int $day = 25;
    public static int $year = 2022;

    public function calcResultString(): string
    {
        $sum = 0;
        foreach ($this->lines as $line) {
            $sum += Snafu::fromSnafuString($line);
        }
        return Snafu::fromIntegerValue($sum);
    }

    public function calcResult2(): int
    {
        throw new AdventOfCodeException('no calcResult2 for this day');
    }

    public function calcResult(): int
    {
        throw new AdventOfCodeException('use calcResultString for this day');
    }
}
