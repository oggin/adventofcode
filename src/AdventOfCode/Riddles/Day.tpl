<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_{Y};

use AdventOfCode\Classes\MainRiddle;
use Unit\_{Y}\TestDay{X};

/**
 * @see TestDay{X};
 * @see ../../../../input/_{Y}/day{X}.txt
 * @see ../../../../tests/Unit/_{Y}/TestDay{X}.php
 */
class Day{X} extends MainRiddle
{
    public static int $day = {X};
    public static int $year = {Y};

    public function calcResult(): int
    {
        return {X} * {X};
    }

    public function calcResult2(): int
    {
         return {X} * {X};
    }

}
