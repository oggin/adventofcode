<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;

class Day6 extends MainRiddle
{
    public static int $day = 6;
    public static int $year = 2020;

    public function calcResult(): int
    {
        $sum = 0;
        foreach (explode(PHP_EOL . PHP_EOL, $this->content) as $answers) {
            $letters = preg_replace('~\s~', '', $answers);
            $sum += count(array_count_values(str_split($letters)));
        }
        return $sum;
    }

    public function calcResult2(): int
    {
        $sum = 0;
        foreach (explode(PHP_EOL . PHP_EOL, $this->content) as $answers) {
            $lines = explode(PHP_EOL, $answers);
            $commonChars = str_split(array_shift($lines));
            foreach ($lines as $line) {
                $commonChars = array_intersect(str_split($line), $commonChars);
            }
            $sum += count($commonChars);
        }
        return $sum;
    }
}
