<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;

class Day3 extends MainRiddle
{
    public static int $day = 3;
    public static int $year = 2020;

    public function calcResult(): int
    {

        return $this->simulate(1, 3);
    }

    public function calcResult2(): int
    {
        return
            $this->simulate(1, 1) *
            $this->simulate(1, 3) *
            $this->simulate(1, 5) *
            $this->simulate(1, 7) *
            $this->simulate(2, 1);
    }

    private function simulate(int $down, int $right): int
    {
        $len = strlen($this->lines[0]);
        $countTrees = 0;
        $colIx = 1;
        for ($rowIdx = $down; $rowIdx < count($this->lines); $rowIdx += $down, $colIx++) {
            $line = $this->lines[$rowIdx];
            $elements = str_split($line);
            $countTrees += $elements[($colIx * $right) % $len] === '#' ? 1 : 0;
        }
        return $countTrees;
    }
}
