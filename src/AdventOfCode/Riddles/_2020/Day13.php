<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2020\Components\Day13\BusStack;

class Day13 extends MainRiddle
{
    public static int $day = 13;
    public static int $year = 2020;

    public function calcResult(): int
    {
        return BusStack::fromInput($this->lines)->getShortestWait();
    }

    public function calcResult2(): int
    {
        return BusStack::fromInput($this->lines)->getSchedule();
    }

}
