<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Classes\Stack;
use AdventOfCode\Riddles\_2020\Components\Bag;

class Day7 extends MainRiddle
{
    public static int $day = 7;
    public static int $year = 2020;

    private int $totalCount = 0;
    private array $mapping = [];
    private ?Stack $bags = null;


    public function calcResult(): int
    {
        $this->init();
        $par = [];
        $this->bags->get('shiny gold')->getNamesOfAllParents($par);
        return count($par) - 1; //own name is contained
    }

    public function calcResult2(): int
    {
        $this->init();
        return $this->getBagAmoutRecurrsive('shiny gold', 1) - 1; //remove the shiny bag;
    }

    private function getBagAmoutRecurrsive($mainBag, $amount): int
    {
        $this->totalCount += $amount;
        foreach ($this->mapping[$mainBag] ?? [] as $child => $childAmout) {
            $this->getBagAmoutRecurrsive($child, $childAmout * $amount);
        }
        return $this->totalCount;
    }

    private function handleChildren(string $childString, Bag $parentBag): void
    {
        $this->bags->add($parentBag->getName(), $parentBag);
        if ($childString === 'no other bags') {
            return;
        }
        $childBagsArray = explode(',', $childString);
        while (($nameAndAmountOfChildBag = array_shift($childBagsArray)) !== null) {
            $nameAndAmountOfChildBag = trim(str_replace(' bags', '', $nameAndAmountOfChildBag));
            $nameAndAmountOfChildBag = trim(str_replace(' bag', '', $nameAndAmountOfChildBag));
            [$amount, $nameOfChildBag] = explode(' ', $nameAndAmountOfChildBag, 2);
            $amount = (int)$amount;

            $this->mapping[$parentBag->getName()][$nameOfChildBag] = $amount;
            $childBag = $this->bags->get($nameOfChildBag) ?? new Bag($nameOfChildBag);
            $childBag->addParent($parentBag);
            $this->bags->add($nameOfChildBag, $childBag);
        }
    }

    private function handleParent(string $line): array
    {
        $line = trim($line, '.');
        $data = explode(' contain ', $line);
        $nameOfParentBag = str_replace(' bags', '', array_shift($data));
        $childString = $data[0];
        $this->mapping[$nameOfParentBag] = [];
        return [$nameOfParentBag, $childString];
    }

    private function init(): void
    {
        $this->bags = new Stack();
        foreach ($this->lines as $line) {
            list($nameOfParentBag, $childString) = $this->handleParent($line);
            $this->handleChildren($childString, $this->bags->get($nameOfParentBag) ?? new Bag($nameOfParentBag));
        }
    }
}
