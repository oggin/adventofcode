<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;

class Day9 extends MainRiddle
{
    public static int $day = 9;
    public static int $year = 2020;
    private int $weakIdx = 0;
    private array $checkedNumbers = [];
    private array $stack = [];

    public function calcResult(): int
    {
        $check = 25;
        $this->stack = [];
        foreach ($this->lines as $idx => $line) {
            $number = (int) $line;
            if ($idx < $check) {
                $this->stack[] = $number;
                continue;
            }

            if (!$this->isValid($check, $number)) {
                $this->weakIdx = $idx;
                return $number;
            }
            array_shift($this->stack);
            $this->stack[] = $number;
        }
        return 0;
    }

    public function calcResult2(): int
    {
        $this->calcResult();

        for ($idx = $this->weakIdx - 1; $idx > 0; $idx--) {
            if ($this->chkDown($idx)) {
                return min($this->checkedNumbers) + max($this->checkedNumbers);
            }
        }

        return 0;
    }

    private function chkDown(int $startIdx): bool
    {
        $toSub = $this->lines[$this->weakIdx];
        $this->checkedNumbers = [];
        $idx = $startIdx;
        while ($idx-- > 0 && $toSub > 0) {
            $toSub -= ($number = (int) $this->lines[$idx]);
            $this->checkedNumbers[] = $number;
        }

        return $toSub === 0;
    }

    private function isValid(int $check, int $number): bool
    {
        for ($i = 0; $i < $check; $i++) {
            for ($j = $i + 1; $j < $check; $j++) {
                if ($this->stack[$i] + $this->stack[$j] === $number) {
                    return true;
                }
            }
        }
        return false;
    }
}
