<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;

class Day10 extends MainRiddle
{
    public static int $day = 10;
    public static int $year = 2020;
    private array $cache = [];
    private int $maxVal = 0;

    public function calcResult(): int
    {
        $stack = $this->getStack();
        $counter = [1 => 0, 3 => 0];

        for ($idx = 0; $idx < count($stack) - 1; $idx++) {
            $counter[$stack[$idx + 1] - $stack[$idx]]++;
        }
        return $counter[1] * $counter[3];
    }

    public function calcResult2(): int
    {
        $stack = $this->getStack();
        $this->maxVal = $stack[count($stack) - 1];
        return $this->findCombinations(0, $stack);
    }

    private function findCombinations(int $currentVal, array $remainingStack): int
    {
        if ($currentVal === $this->maxVal) {
            return 1;
        }
        $count = 0;
        for ($i = 1; $i <= 3; $i++) {
            if (in_array(($nextEnd = $currentVal + $i), $remainingStack)) {
                $this->cache[$nextEnd] ??= $this->findCombinations($nextEnd, array_filter($remainingStack, function ($val) use ($nextEnd) {
                    return $val > $nextEnd;
                }));
                $count += $this->cache[$nextEnd];
            }
        }
        return $count;
    }

    private function getStack(): array
    {
        $stack = [-3, 0];
        foreach ($this->lines as $line) {
            $stack[] = (int)$line;
        }
        sort($stack);
        return $stack;
    }
}
