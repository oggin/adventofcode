<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;

class Day5 extends MainRiddle
{
    public static int $day = 5;
    public static int $year = 2020;

    public function calcResult(): int
    {
        $highest = 0;
        foreach ($this->lines as $line) {
            $rowString = substr($line, 0, 7);
            $seatString = substr($line, -3);
            $row = $this->halfPart(0, pow(2, strlen($rowString)) - 1, str_split($rowString), 'F', 'B');
            $seat = $this->halfPart(0, pow(2, strlen($seatString)) - 1, str_split($seatString), 'L', 'R');
            $current = $row * 8 + $seat;
            $highest = max($highest, $current);
        }

        return $highest;
    }

    public function calcResult2(): int
    {
        $seatIds = [];
        foreach ($this->lines as $line) {
            $rowString = substr($line, 0, 7);
            $seatString = substr($line, -3);
            $row = $this->halfPart(0, pow(2, strlen($rowString)) - 1, str_split($rowString), 'F', 'B');
            $seat = $this->halfPart(0, pow(2, strlen($seatString)) - 1, str_split($seatString), 'L', 'R');
            $current = $row * 8 + $seat;
            $seatIds[] = $current;
        }

        sort($seatIds);
       for($idx = 0; $idx < count($seatIds) -1; $idx++) {
           if($seatIds[$idx+1] - $seatIds[$idx] === 2) {
               return $seatIds[$idx]+1;
           }
       }
      return 3;
    }

    private function halfPart(int $lowerBound, int $upperBound, array $sequence, string $charLow, string $charHigh): int
    {

        if (count($sequence) === 1) {
            return $sequence[0] === $charLow ? $lowerBound : $upperBound;
        }

        $char = array_shift($sequence);
        $bound = (int)(($upperBound + 1 - $lowerBound) / 2);
        if ($char === $charLow) {
            $upperBound -= $bound;
        } else {
            $lowerBound += $bound;
        }
        return $this->halfPart($lowerBound, $upperBound, $sequence, $charLow, $charHigh);
    }
}
