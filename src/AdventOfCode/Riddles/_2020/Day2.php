<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2020\Components\Password;

class Day2 extends MainRiddle
{
    public static int $day = 2;
    public static int $year = 2020;
    private int $validCount = 0;

    public function calcResult(): int
    {
        foreach ($this->lines as $line) {
            $this->validCount += Password::fromRegex($line)->hasMinAndMaxAmountOfChar() ? 1 : 0;
        }
        return $this->validCount;
    }

    public function calcResult2(): int
    {
        foreach ($this->lines as $line) {
            $this->validCount += Password::fromRegex($line)->hasExactOneMatchingCharAtPosition() ? 1 : 0;
        }
        return $this->validCount;
    }
}
