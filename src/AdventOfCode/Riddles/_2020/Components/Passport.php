<?php

namespace AdventOfCode\Riddles\_2020\Components;

use AdventOfCode\Exception\InputException;

class Passport
{
    private ?int $byr = null;
    private ?int $eyr = null;
    private ?string $pid = null;
    private ?string $hgt = null;
    private ?int $iyr = null;
    private ?string $ecl = null;
    private ?string $hcl = null;


    /**
     * @throws InputException
     */
    public static function fromString(string $data): Passport
    {
        $input = preg_split('~\s~', $data);
        $passport = new self();
        foreach ($input as $dataSet) {
            [$key, $value] = explode(':', $dataSet);
            match ($key) {
                'byr' => $passport->byr = (int)$value,
                'eyr' => $passport->eyr = (int)$value,
                'iyr' => $passport->iyr = (int)$value,
                'pid' => $passport->pid = $value,
                'hgt' => $passport->hgt = $value,
                'ecl' => $passport->ecl = $value,
                'hcl' => $passport->hcl = $value,
                'cid' => null,//not needed
                default => throw new InputException('invalid passport data')
            };
        }
        return $passport;
    }

    public function isValid(): bool
    {
        return
            $this->byr !== null &&
            $this->ecl !== null &&
            $this->eyr !== null &&
            $this->hcl !== null &&
            $this->hgt !== null &&
            $this->iyr !== null &&
            $this->pid !== null;
    }

    public function isValidStrict(): bool
    {
        return
            $this->byr >= 1920 && $this->byr <= 2002 && //byr (Birth Year) - four digits; at least 1920 and at most 2002.
            $this->iyr >= 2010 && $this->iyr <= 2020 && //iyr (Issue Year) - four digits; at least 2010 and at most 2020.
            $this->eyr >= 2020 && $this->eyr <= 2030 && // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
            $this->validateHeight() &&
            preg_match('~^#[a-f0-9]{6}$~', $this->hcl) === 1 &&  //hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
            preg_match('~^\d{9}$~', $this->pid) === 1 &&  //pid (Passport ID) - a nine-digit number, including leading zeroes.
            in_array($this->ecl, ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']) // ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
            ;
    }

    private function validateHeight(): bool
    {

        if (preg_match('~^\d+cm|in~', $this->hgt) !== 1) {
            return false;
        }  //hgt (Height) - a number followed by either cm or in:

        $height = (int)$this->hgt;
        if (str_ends_with($this->hgt, 'cm')) {
            return $height >= 150 && $height <= 193; // If cm, the number must be at least 150 and at most 193.
        }
        return $height >= 59 && $height <= 76; // If in, the number must be at least 59 and at most 76
    }
}
