<?php

namespace AdventOfCode\Riddles\_2020\Components;

use AdventOfCode\Exception\InputException;

class Password
{
    private string $password;
    private string $char;

    private int $min;
    private int $max;

    /**
     * @throws InputException
     */
    public static function fromRegex(string $line): Password
    {
        if (!preg_match('~^(?<min>\d+)-(?<max>\d+)\s(?<char>[a-z]):\s(?<password>.*)$~', $line, $match)) {
            throw new InputException('invalid input ' . $line);
        }

        $password = new self();
        $password->password = $match['password'];
        $password->char = $match['char'];
        $password->min = (int)$match['min'];
        $password->max = (int)$match['max'];
        return $password;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getChar(): string
    {
        return $this->char;
    }

    public function getMin(): int
    {
        return $this->min;
    }

    public function getMax(): int
    {
        return $this->max;
    }

    public function hasMinAndMaxAmountOfChar(): bool
    {
        $charCount = substr_count($this->password, $this->char);
        return $charCount >= $this->min && $charCount <= $this->max;
    }

    public function hasExactOneMatchingCharAtPosition(): bool
    {
        $passwordStr = str_split($this->password);
        $firstMatch = $passwordStr[$this->min - 1] === $this->char;
        $secondMatch = $passwordStr[$this->max - 1] === $this->char;
        return (bool)($firstMatch ^ $secondMatch);
    }
}
