<?php

namespace AdventOfCode\Riddles\_2020\Components;

class Command
{
    private string $action;
    private int $number;

    public static function fromString($string): Command
    {
        [$action, $number] = explode(' ', $string);
        $command = new self();
        $command->action = $action;
        $command->number = (int)$number;
        return $command;
    }


    public function isJump(): bool
    {
        return $this->action === 'jmp';
    }

    public function isACC(): bool
    {
        return $this->action === 'acc';
    }


    public function getNumber(): int
    {
        return $this->number;
    }

    public function toggleJmpNop(): bool
    {
        if ($this->isACC()) {
            return false;
        }
        if ($this->isJump()) {
            $this->action = 'nop';
        } else {
            $this->action = 'jmp';
        }
        return true;
    }
}
