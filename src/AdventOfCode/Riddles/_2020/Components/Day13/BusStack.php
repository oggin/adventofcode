<?php

namespace AdventOfCode\Riddles\_2020\Components\Day13;

class BusStack
{
    /**
     * @var Bus[];
     */
    private array $buses = [];
    private int $departure = 0;

    public static function fromInput(array $lines): BusStack
    {
        $instance = new self();
        $instance->departure = (int)$lines[0];

        $instance->setBuses($lines);
        return $instance;

    }

    public function getShortestWait(): int
    {
        $departure = $this->departure;
        usort($this->buses, static fn(Bus $a, Bus $b) => $b->waitInMinutes($departure) -
            $a->waitInMinutes($departure));
        $shortestWaitBus = array_pop($this->buses);
        return $shortestWaitBus->waitInMinutes($this->departure) * $shortestWaitBus->getNumber();
    }

    private function setBuses(array $lines): void
    {
        foreach (explode(',', $lines[1]) as $idx => $busString) {
            if ($busString === 'x') {
                continue;
            }

            $this->buses[] = Bus::fromNumberAndIndex((int)$busString, $idx);
        }

    }

    public function getSchedule()
    {
        //sort buses by number, DESC
        usort($this->buses, static fn(Bus $a, Bus $b) => $b->getNumber() - $a->getNumber());

        //first bus, try to find matching times
        $startBus = array_shift($this->buses);
        $stepWidth = 1;

        for ($i = 1; $i < PHP_INT_MAX; $i += $stepWidth) {
            $time = $startBus->getTimeBasedOnZeroIndex($i);
            $match = true;
            foreach ($this->buses as $bus) {
                if ($bus->matchIndexOffsetByTime($time)) {
                    $stepWidth = $bus->handleStepWidth($i) ?? $stepWidth;
                } else {
                    $match = false;
                    break;
                }
            }

            if ($match) {
                return $time;
            }
        }

        return 0;
    }
}