<?php

namespace AdventOfCode\Riddles\_2020\Components\Day13;

class Bus
{
    private int $number;
    private int $index;
    private ?int $stepBasedOnIndex = null;
    private ?int $firstMatchOnIndex = null;

    public static function fromNumberAndIndex(int $number, int $index): Bus
    {
        $instance = new self();
        $instance->number = $number;
        $instance->index = $index;
        return $instance;
    }

    public function waitInMinutes(int $departure): int
    {
        return $this->number - ($departure % $this->number);
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getIndex(): int
    {
        return $this->index;
    }

    public function getTimeBasedOnZeroIndex(int $factor): int
    {
        return $factor * $this->getNumber() - $this->getIndex();
    }

    public function matchIndexOffsetByTime(int $time): bool
    {
        return ($time + $this->getIndex()) % $this->getNumber() === 0;
    }

    public function getStepBasedOnIndex(): ?int
    {
        return $this->stepBasedOnIndex;
    }

    public function setStepBasedOnIndex(?int $stepBasedOnIndex): void
    {
        $this->stepBasedOnIndex = $stepBasedOnIndex;
    }

    public function getFirstMatchOnIndex(): ?int
    {
        return $this->firstMatchOnIndex;
    }

    public function setFirstMatchOnIndex(int $firstMatchOnIndex): void
    {
        $this->firstMatchOnIndex = $firstMatchOnIndex;
    }

    public function handleStepWidth($currentIndex): ?int
    {
        $firstMatchOnIndex = $this->getFirstMatchOnIndex();
        if ($firstMatchOnIndex === null && $this->getStepBasedOnIndex() === null) {
            $this->setFirstMatchOnIndex($currentIndex);
            return null;
        }

        if ($this->getStepBasedOnIndex() === null) {
            $this->setStepBasedOnIndex($currentIndex - $firstMatchOnIndex);
            return $this->getStepBasedOnIndex();
        }

        return null;
    }




}