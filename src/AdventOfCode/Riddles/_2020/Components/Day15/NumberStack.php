<?php

namespace AdventOfCode\Riddles\_2020\Components\Day15;

class NumberStack
{
    private int $lastNumber = 0;
    private int $startIndex = 0;
    /** @var NumberData[] */
    private array $stack = [];

    public function getLastNumber(int $turns): int
    {
        for ($index = $this->startIndex; $index < $turns; $index++) {
            $this->calculateNumberByIndex($index);
        }
        return $this->lastNumber;
    }

    public static function fromInput(string $input): NumberStack
    {
        $input = explode(',', $input);
        $instance = new self();
        $instance->startIndex = count($input);
        $instance->lastNumber = (int)end($input);
        foreach ($input as $idx => $number) {
            $instance->setNumberIndex((int)$number, $idx);
        }
        return $instance;
    }

    public function setNumberIndex(int $number, int $index): void
    {
        $this->stack['#' . $number] ??= new NumberData();
        $this->stack['#' . $number]->update($index);

    }

    public function calculateNumberByIndex(int $index): void
    {
        $this->lastNumber = $this->stack['#' . $this->lastNumber]->getIndexDifference($index);
        $this->setNumberIndex($this->lastNumber, $index);
    }

}