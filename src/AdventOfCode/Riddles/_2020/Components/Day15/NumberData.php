<?php

namespace AdventOfCode\Riddles\_2020\Components\Day15;

class NumberData
{
    private ?int $preLast = null;
    private ?int $last = null;

    public function update(int $index): void
    {
        $this->preLast = $this->last;
        $this->last = $index;

    }

    public function getIndexDifference(int $index): ?int
    {
        return $this->preLast === null && $this->last === $index - 1 ? 0 : $this->last - $this->preLast;
    }
}