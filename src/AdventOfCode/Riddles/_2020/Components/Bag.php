<?php

namespace AdventOfCode\Riddles\_2020\Components;

class Bag
{
    /**
     * @var array<string,Bag>
     */
    private array $parent = [];
    private array $children = [];

    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function addParent(Bag $parent): void
    {
        if (!isset($this->parent[$parent->name])) {
            $this->parent[$parent->name] = $parent;
            $parent->addChild($this);
        }
    }

    public function addChild(Bag $child): void
    {
        if (!isset($this->children[$child->name])) {
            $this->children[$child->name] = $child;
            $child->addParent($this);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNamesOfAllParents(&$par): void
    {

        $par[$this->name] = $this->name;
        foreach ($this->parent as $parent) {
            $parent->getNamesOfAllParents($par);
        }
    }
}
