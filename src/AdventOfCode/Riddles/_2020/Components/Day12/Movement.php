<?php

namespace AdventOfCode\Riddles\_2020\Components\Day12;

class Movement
{
    private int $xChange = 0;
    private int $yChange = 0;

    /**
     * Action N means to move north by the given value.
     * Action S means to move south by the given value.
     * Action E means to move east by the given value.
     * Action W means to move west by the given value.
     */
    public static function fromString(string $movement): ?Movement
    {
        if(!self::isMovement($movement)) {
            return null;
        }
        $direction = substr($movement, 0, 1);
        $distance = (int)substr($movement, 1);
        $instance = new self();
        $instance->xChange =
            match ($direction) {
                'E' => $distance,
                'W' => $distance * -1,
                default => 0
            };

        $instance->yChange =
            match ($direction) {
                'N' => $distance,
                'S' => $distance * -1,
                default => 0
            };

        return $instance;
    }

    private static function isMovement(string $string): bool
    {
        return in_array(substr($string, 0, 1), ['N', 'E', 'S', 'W']);
    }

    public function getXChange(): int
    {
        return $this->xChange;
    }

    public function getYChange(): int
    {
        return $this->yChange;
    }


}