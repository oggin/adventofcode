<?php

namespace AdventOfCode\Riddles\_2020\Components\Day12;

class Ferry
{
    private int $x = 0;
    private int $y = 0;

    private Navigation $navigation;

    public function __construct(Navigation $navigation)
    {
        $this->navigation = $navigation;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function updateX(int $x): void
    {
        $this->x += $x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function updateY(int $y): void
    {
        $this->y += $y;
    }

    public function updateCourse(string $command): void
    {
        $this->navigation->rotate(Rotation::fromString($command), $this);
        $this->navigation->move(Movement::fromString($command), $this);
        $this->navigation->forward(Forward::fromString($command), $this);
    }


}