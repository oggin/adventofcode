<?php

namespace AdventOfCode\Riddles\_2020\Components\Day12;

class Forward
{
    private int $distance;

    public static function fromString(string $string): ?Forward
    {
        if (!self::isForward($string)) {
            return null;
        }
        $instance = new self();
        $instance->distance = (int)substr($string, 1);
        return $instance;
    }

    private static function isForward(string $string): bool
    {
        return str_starts_with($string, 'F');
    }

    public function getDistance(): int
    {
        return $this->distance;
    }


}