<?php

namespace AdventOfCode\Riddles\_2020\Components\Day12;

interface Navigation
{

    public function forward(?Forward $forward, Ferry $ferry);
    public function rotate(?Rotation $turn, Ferry $ferry);
    public function move(?Movement $movement, Ferry $ferry);

}