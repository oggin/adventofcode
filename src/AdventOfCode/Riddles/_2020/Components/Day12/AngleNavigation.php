<?php

namespace AdventOfCode\Riddles\_2020\Components\Day12;

class AngleNavigation implements Navigation
{
    /**
     *  0 N
     * 45 NE
     * 90 E
     * 135 SE
     * 180 S
     * 225 SW
     * 270 W
     * 315 NW
     **/
    private int $angle;

    private function __construct()
    {

    }

    public static function fromAngle(int $angle): AngleNavigation
    {
        $instance = new self();
        $instance->angle = $angle;
        return $instance;
    }
    

    public function rotate(?Rotation $turn, Ferry $ferry): void
    {
        if (($newAngle = $this->angle + $turn?->getAngle() ?? 00) < 0) {
            $newAngle +=  360;
        }
        $this->angle = $newAngle % 360;
    }

    public function forward(?Forward $forward, $ferry): void
    {
        $distance = $forward?->getDistance() ?? 0;
        $ferry->updateY($this->getYChange($distance));
        $ferry->updateX($this->getXChange($distance));
    }

    private function facingNorth(): bool
    {
        return in_array($this->angle, [315, 0, 45]);
    }

    private function facingSouth(): bool
    {
        return in_array($this->angle, [135, 180, 225]);
    }

    private function facingEast(): bool
    {
        return in_array($this->angle, [45, 90, 135]);
    }

    private function facingWest(): bool
    {
        return in_array($this->angle, [225, 270, 315]);
    }

    public function move(?Movement $movement, Ferry $ferry): void
    {
        $ferry->updateX($movement?->getXChange() ?? 0);
        $ferry->updateY($movement?->getYChange() ?? 0);
    }

    public function getYChange(int $distance): int
    {
        return match (true) {
            $this->facingNorth() => $distance,
            $this->facingSouth() => $distance * -1,
            default => 0
        };
    }

    public function getXChange(int $distance): int
    {
        return match (true) {
            $this->facingEast() => $distance,
            $this->facingWest() => $distance * -1,
            default => 0
        };
    }
}