<?php

namespace AdventOfCode\Riddles\_2020\Components\Day12;

class WaypointNavigation implements Navigation
{
    private int $x;
    private int $y;

    public function __construct(int $x, int $y) {
        $this->x = $x;
        $this->y = $y;
    }

    public function rotateCounterClockWise(int $angleInDegree): void
    {
        $angleInRad = deg2rad($angleInDegree);
        $cos = (int)cos($angleInRad);
        $sin = (int)sin($angleInRad);

        $newX = $cos * $this->x - $sin * $this->y;
        $newY = $sin * $this->x + $cos * $this->y;

        $this->x = $newX;
        $this->y = $newY;

    }

    public function move(?Movement $movement, Ferry $ferry): void
    {
        $this->x += $movement?->getXChange() ?? 0;
        $this->y += $movement?->getYChange() ?? 0;
    }

    public function rotate(?Rotation $turn, Ferry $ferry): void
    {
       $this->rotateCounterClockWise($turn?->getAngleCounterClockWise() ?? 0);

    }
    public function forward(?Forward $forward, $ferry): void
    {
        $distance = $forward?->getDistance() ?? 0;
        $ferry->updateX($this->x * $distance);
        $ferry->updateY($this->y * $distance);

    }

}