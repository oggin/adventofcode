<?php

namespace AdventOfCode\Riddles\_2020\Components\Day12;

class Rotation
{
    private int $angle;
    private int $angleCounterClockWise;

    public static function fromString(string $change): ?Rotation
    {
        if (!self::isTurn($change)) {
            return null;
        }

        $direction = substr($change, 0, 1);
        $angle = (int)substr($change, 1);
        $instance = new self();
        $instance->angle =
            match ($direction) {
                'L' => 360 - $angle,
                'R' => $angle,
            };

        $instance->angleCounterClockWise =
            match ($direction) {
                'L' => $angle,
                'R' => 360 - $angle,
            };

        return $instance;
    }

    public function getAngle(): int
    {
        return $this->angle;
    }

    public function getAngleCounterClockWise(): int
    {
        return $this->angleCounterClockWise;
    }

    private static function isTurn(string $string): bool
    {
        return in_array(substr($string, 0, 1), ['L', 'R']);
    }


}