<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2020\Components\Command;

class Day8 extends MainRiddle
{
    public static int $day = 8;
    public static int $year = 2020;
    private int $acc = 0;
    private int $idxToModify = -1;
    /**
     * @var Command[]
     */
    private array $commands = [];

    public function calcResult(): int
    {
        $this->initCommands();
         $this->simulate();
         return $this->acc;
    }

    private function initCommands()
    {
        foreach ($this->lines as $line) {
            $this->commands[] = Command::fromString($line);
        }
    }

    public function calcResult2(): int
    {
        $this->initCommands();
        $num = 0;
        $numOfCommands = count($this->commands);
        if ($this->simulate() === true) {
            return $this->acc;
        }
        while ($this->idxToModify < $numOfCommands) {
            do {
                $this->idxToModify++;
            } while ($this->idxToModify < $numOfCommands && $this->commands[$this->idxToModify]->toggleJmpNop() === false);

            if ($this->simulate() === true) {
                return $this->acc;
            }
            $this->commands[$this->idxToModify]->toggleJmpNop();


            if ($num++ > 100000) {
                return 0;
            }
        }

        return -1;
    }

    private function simulate(): bool
    {
        $executedLines = [0];
        $this->acc = 0;
        $curIdx = 0;
        $command = $this->commands[$curIdx];
        do {
            if ($command->isJump()) {
                $curIdx += $command->getNumber();
            } else {
                if ($command->isACC()) {
                    $this->acc += $command->getNumber();
                }
                $curIdx++;
            }
            if (in_array($curIdx, $executedLines)) {
                return false;
            }
            $executedLines[] = $curIdx;

            if (($command = $this->commands[$curIdx] ?? null) === null) {
                return true;
            }
        } while ($curIdx < count($this->commands));


        return true;
    }
}
