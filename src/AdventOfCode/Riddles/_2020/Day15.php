<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2020\Components\Day15\NumberStack;

class Day15 extends MainRiddle
{
    public static int $day = 15;
    public static int $year = 2020;

    public function calcResult(): int
    {
        return NumberStack::fromInput($this->lines[0])->getLastNumber(2020);
    }

    public function calcResult2(): int
    {
        return NumberStack::fromInput($this->lines[0])->getLastNumber(30000000);

    }


}
