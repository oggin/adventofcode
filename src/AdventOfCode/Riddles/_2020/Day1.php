<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Classes\Stack;

class Day1 extends MainRiddle
{
    public static int $day = 1;
    public static int $year = 2020;
    private ?Stack $stack = null;

    public function calcResult(): int
    {
        $this->stack = new Stack();
        foreach ($this->lines as $line) {
            $number = (int)$line;
            if ($this->stack->has('#' . $number)) {
                return $this->stack->get('#' . $number) * $number;
            }
            $this->stack->add('#' . (self::$year - $number), $number);
        }
        return 0;
    }

    public function calcResult2(): int
    {
        $this->stack = new Stack();
        $numbers = [$line0 = (int)$this->lines[0], $line1 = (int)$this->lines[1]];
        $this->stack->add('#' . (self::$year - $line0 - $line1), [$line0, $line1]);
        for ($idx = 2; $idx < count($this->lines); $idx++) {
            $number = (int)$this->lines[$idx];
            if ($this->stack->has('#' . $number)) {
                return $this->stack->get('#' . $number)[0] * $this->stack->get('#' . $number)[1] * $number;
            }
            for ($lookUp = count($numbers) - 1; $lookUp >= 0; $lookUp--) {
                $this->stack->add('#' . (self::$year - $number - $numbers[$lookUp]), [$number, $numbers[$lookUp]]);
            }
            $numbers[] = $number;
        }
        return 0;
    }
}
