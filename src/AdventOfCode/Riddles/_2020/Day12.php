<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2020\Components\Day12\AngleNavigation;
use AdventOfCode\Riddles\_2020\Components\Day12\Ferry;
use AdventOfCode\Riddles\_2020\Components\Day12\WaypointNavigation;

class Day12 extends MainRiddle
{
    public static int $day = 12;
    public static int $year = 2020;

    public function calcResult(): int
    {
        return $this->cruise(new Ferry(AngleNavigation::fromAngle(90)));
    }

    public function calcResult2(): int
    {
        return $this->cruise(new Ferry(new WaypointNavigation(10, 1)));
    }

    public function cruise(Ferry $ferry): int|float
    {
        foreach ($this->lines as $line) {
            $ferry->updateCourse($line);
        }
        return (int)(abs($ferry->getX()) + abs($ferry->getY()));
    }


}
