<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2020;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2020\Components\Passport;

class Day4 extends MainRiddle
{
    public static int $day = 4;
    public static int $year = 2020;

    public function calcResult(): int
    {
        $validCnt = 0;
        foreach (explode(PHP_EOL . PHP_EOL, $this->content) as $passport) {
            $validCnt += Passport::fromString($passport)->isValid() ? 1 : 0;
        }

        return $validCnt;
    }

    public function calcResult2(): int
    {
        $validCnt = 0;
        foreach (explode(PHP_EOL . PHP_EOL, $this->content) as $passport) {
            $validCnt += Passport::fromString($passport)->isValidStrict() ? 1 : 0;
        }

        return $validCnt;
    }
}
