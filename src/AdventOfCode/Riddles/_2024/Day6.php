<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2024;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Classes\Maze;
use AdventOfCode\Classes\NewMaze;
use Unit\_2024\TestDay6;

/**
 * @see TestDay6;
 * @see ../../../../input/_2024/day6.txt
 * @see ../../../../tests/Unit/_2024/TestDay6.php
 */
class Day6 extends MainRiddle
{
    public static int $day = 6;
    public static int $year = 2024;

    public function calcResult(): int
    {
        $maze = new NewMaze($this->lines);

        return $maze->isGuardTrappedInLoop();

    }

    public function calcResult2(): int
    {
        //$maze = new Maze($this->lines);
        //return $maze->calcResult2();

        $m =new NewMaze($this->lines);
        return  $m->countGuardInfiniteLoops();
    }

}
