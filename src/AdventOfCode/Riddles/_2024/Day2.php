<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2024;

use AdventOfCode\Classes\Helper;
use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Classes\Number;
use Unit\_2024\TestDay2;

/**
 * @see TestDay2;
 * @see ../../../../input/_2024/day2.txt
 */
class Day2 extends MainRiddle
{
    public static int $day = 2;
    public static int $year = 2024;

    public function calcResult(): int
    {
        $validCount = 0;
        foreach ($this->lines as $line) {
            $validCount += $this->checkLine(Helper::castElementsToInt(explode(' ', $line))) ? 1 : 0;
        }
        return $validCount;
    }

    public function calcResult2(): int
    {
        $validCount = 0;
        foreach ($this->lines as $line) {
            $perLine = Helper::castElementsToInt(explode(' ', $line));
            $isValid = false;
            for($i = 0; $i < count($perLine); $i++) {
                $tmp = $perLine;
                unset($tmp[$i]);
                $result = array_values($tmp);
                if($isValid = $this->checkLine($result)) {
                    break;
                }
            }
            $validCount += $isValid ? 1 : 0;
        }
        return $validCount;
    }


    private function checkLine(array $perLine): bool
    {
        $firstNumber = new Number($perLine[0]);
        $secondNumber = new Number($perLine[1]);
        if (($order = $firstNumber->compareOrder($secondNumber)) === 0 || !$firstNumber->compareDifference($secondNumber)) {
           return false;
        }
        $lastNumber = $secondNumber;
        for ($idx = 2; $idx < count($perLine); $idx++) {
            $number = $perLine[$idx];
            $currentNumber = new Number($number);
            if (!($currentNumber->compare($lastNumber, $order))) {
                return false;
            }

            $lastNumber = $currentNumber;
        }
        return true;
    }

}
