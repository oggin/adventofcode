<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2024;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Classes\Printer;
use Unit\_2024\TestDay5;

/**
 * @see TestDay5;
 * @see ../../../../input/_2024/day5.txt
 */
class Day5 extends MainRiddle
{
    public static int $day = 5;
    public static int $year = 2024;

    public function calcResult(): int
    {
        return Printer::create($this->lines)->check();
    }

    public function calcResult2(): int
    {
        return Printer::create($this->lines)->check2();
    }

}
