<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2024;

use AdventOfCode\Classes\MainRiddle;
use Unit\_2024\TestDay1;

/**
 * @see TestDay1;
 * @see ../../../../input/_2024/day1.txt
 */
class Day1 extends MainRiddle
{
    public static int $day = 1;
    public static int $year = 2024;
    private array $leftStack = [];
    private array $rightStackComplete = [];
    private array $rightStackCount = [];

    public function calcResult(): int
    {
        $this->initStacks();
        sort($this->leftStack);
        sort($this->rightStackComplete);

        $diff = 0;
        for ($i = 0; $i < count($this->leftStack); $i++) {
            $diff += abs($this->leftStack[$i] - $this->rightStackComplete[$i]);
        }
        return $diff;
    }

    public function calcResult2(): int
    {
        $this->initStacks();
        $diff = 0;
        for ($i = 0; $i < count($this->leftStack); $i++) {
            $diff += ($this->rightStackCount['#' . $this->leftStack[$i]] ?? 0) * $this->leftStack[$i];
        }
        return $diff;
    }

    /**
     * @return void
     */
    private function initStacks(): void
    {
        foreach ($this->lines as $line) {
            $tmp = explode('   ', $line);
            $this->leftStack[] = (int)$tmp[0];
            $secondNumber = (int)$tmp[1];
            $this->rightStackComplete[] = $secondNumber;
            $this->rightStackCount['#' . $secondNumber] = ($this->rightStackCount['#' . $secondNumber] ?? 0) + 1;
        }
    }

}
