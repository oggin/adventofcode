<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2024;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Classes\Xmas;
use Unit\_2024\TestDay4;

/**
 * @see TestDay4;
 * @see ../../../../input/_2024/day4.txt
 * @see ../../../../tests/Unit/_2024/TestDay4.php
 */
class Day4 extends MainRiddle
{
    public static int $day = 4;
    public static int $year = 2024;

    public function calcResult(): int
    {
        return Xmas::fromLines($this->lines)->countXmasOccurrences("XMAS");
    }

    public function calcResult2(): int
    {
        return Xmas::fromLines($this->lines)->countMasOccurrences();
    }

}
