<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2024;

use AdventOfCode\Classes\Helper;
use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2024\Components\TreeNodeAddition;
use AdventOfCode\Riddles\_2024\Components\TreeNodeCalculationInterface;
use AdventOfCode\Riddles\_2024\Components\TreeNodeConcatination;
use AdventOfCode\Riddles\_2024\Components\TreeNode;
use AdventOfCode\Riddles\_2024\Components\TreeNodeMultiplication;
use Unit\_2024\TestDay7;

/**
 * @see TestDay7;
 * @see ../../../../input/_2024/day7.txt
 * @see ../../../../tests/Unit/_2024/TestDay7.php
 */
class Day7 extends MainRiddle
{
    public static int $day = 7;
    public static int $year = 2024;

    public function calcResult(): int
    {
        $sum = new TreeNodeAddition();
        $prod = new TreeNodeMultiplication();
        return $this->solve($sum, $prod);
    }

    public function calcResult2(): int
    {
        $sum = new TreeNodeAddition();
        $prod = new TreeNodeMultiplication();
        $con = new TreeNodeConcatination();
        return $this->solve($sum, $prod, $con);
    }

    private function solve(TreeNodeCalculationInterface ...$treeNodeCalculation): int
    {
        $sumOfCorrectNumbers = 0;
        foreach ($this->lines as $line) {
            $row = explode(':', $line);
            $targetValue = (int)$row[0];
            $numbers = Helper::castElementsToInt(explode(' ', trim($row[1])));
            $first = array_shift($numbers);
            $tree = new TreeNode($first, $numbers, ...$treeNodeCalculation);
            $sumOfCorrectNumbers += $tree->compareLeaves($targetValue) ? $targetValue : 0;
        }

        return $sumOfCorrectNumbers;
    }

}
