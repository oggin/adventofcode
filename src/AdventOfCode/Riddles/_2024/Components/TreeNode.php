<?php

namespace AdventOfCode\Riddles\_2024\Components;

class TreeNode
{
    public int $value;
    private array $children = [];

    /**
     * @param int $currentValue of node
     * @param array $numbers left in tree to check
     * @param TreeNodeCalculationInterface ...$calculation list of calculations to apply (sum, multiplication, etc.)
     */
    public function __construct(int $currentValue, array $numbers, TreeNodeCalculationInterface ...$calculation)
    {
        $this->value = $currentValue;

        if (empty($numbers)) {
            return;
        }

        $nextValue = array_shift($numbers);

        foreach ($calculation as $calc) {
            $this->children[] = new TreeNode($calc->nextValue($currentValue, $nextValue), $numbers, ...$calculation);
        }

    }

    /**
     * @param int $targetValue
     * @return bool at least one leaf has the target value
     */
    public function compareLeaves(int $targetValue): bool
    {
        if (count($this->children) === 0) {
            return $this->value == $targetValue; // Zielwert gefunden
        }

        foreach ($this->children as $child) {
            if ($child->compareLeaves($targetValue)) {
                return true;
            }
        }
       return false;
    }
}