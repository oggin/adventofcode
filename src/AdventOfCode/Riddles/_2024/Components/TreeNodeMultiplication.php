<?php

namespace AdventOfCode\Riddles\_2024\Components;

class TreeNodeMultiplication implements TreeNodeCalculationInterface
{

    public function nextValue(int $currentValue, int $nextValue): int
    {
        return $currentValue * $nextValue;
    }
}