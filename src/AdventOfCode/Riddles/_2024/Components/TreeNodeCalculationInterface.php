<?php

namespace AdventOfCode\Riddles\_2024\Components;

/**
 * defines the calculation to be performed on the tree nodes
 */
interface TreeNodeCalculationInterface
{
    public function nextValue(int $currentValue, int $nextValue): int;
}