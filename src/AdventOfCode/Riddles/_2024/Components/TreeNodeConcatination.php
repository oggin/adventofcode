<?php

namespace AdventOfCode\Riddles\_2024\Components;

class TreeNodeConcatination implements TreeNodeCalculationInterface
{

    public function nextValue(int $currentValue, int $nextValue): int
    {
        return (int)($currentValue . $nextValue);
    }
}