<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2024;

use AdventOfCode\Classes\MainRiddle;
use PHPUnit\Framework\Assert;
use Unit\_2024\TestDay3;

/**
 * @see TestDay3;
 * @see ../../../../input/_2024/day3.txt
 */
class Day3 extends MainRiddle
{
    public static int $day = 3;
    public static int $year = 2024;
    private int $sum = 0;

    public function calcResult(): int
    {
        $this->matchOne($this->content);
        return $this->sum;
    }

    public function calcResult2(): int
    {
        $this->multiplyAndSum($this->content);
        $solutionFromReddit = $this->sum;
        $this->sum = 0;
        $this->mySolution();
        Assert::assertEquals($solutionFromReddit, $this->sum);
        return $this->sum;

    }

    private function matchOne(string $content): void
    {
        preg_match_all('~mul\(\d{1,3},\d{1,3}\)~', $content, $matches);

        foreach ($matches[0] as $match) {
            $tmp = explode(',', $match);
            $factor1 = (int)str_replace('mul(', '', $tmp[0]);
            $factor2 = (int)$tmp[1];
            $this->sum += $factor1 * $factor2;
        }

    }

    /**
     * found on reddit
     */
    private function multiplyAndSum(string $value): void
    {
        $regex = "~mul\\(\\d+,\\d+\\)|do\\(\\)|don't\\(\\)~";
        preg_match_all($regex, $value, $matches);

        $enabled = true;
        foreach ($matches[0] as $match) {

            if ($match === "do()") {
                $enabled = true;
            } else
                if ($match === "don't()") {
                    $enabled = false;
                }

            if ($enabled && str_starts_with($match, "mul(")) {
                $this->matchOne($match);

            }
        }
    }

    private function mySolution(): void
    {
        $nextStop = strpos($this->content, "don't()");
        $this->matchOne(substr($this->content, 0, $nextStop));
        $restString = substr($this->content, $nextStop);

        preg_match_all("~do\(\).*?don\'t\(\)|do\(\).*~", $restString, $matches);
        foreach ($matches[0] as $match) {
            $this->matchOne($match);
        }
    }
}
