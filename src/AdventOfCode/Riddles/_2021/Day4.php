<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2021;

use AdventOfCode\Classes\Helper;
use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2021\Components\Day4\BingoBoard;

class Day4 extends MainRiddle
{
    public static int $day = 4;
    public static int $year = 2021;

    /**
     * @var array<int,BingoBoard>
     */
    private array $boards;
    private array $numbers;

    public function __construct()
    {
        parent::__construct();
        $this->numbers = Helper::castElementsToInt(explode(',', $this->lines[0]));
        $lineCnt = count($this->lines);
        for ($idx = 2; $idx <= $lineCnt - 5; $idx += 6) {
            $this->boards[] = new BingoBoard(array_slice($this->lines, $idx, 5));
        }
    }

    public function calcResult(): int
    {
        foreach ($this->numbers as $number) {
            foreach ($this->boards as $board) {
                if ($board->markValue($number)) {
                    return $board->getSumUnChecked() * $number;
                }
            }
        }

        return -1;
    }

    public function calcResult2(): int
    {
        $return = false;
        foreach ($this->numbers as $number) {
            $remainingBoards = [];
            foreach ($this->boards as $board) {
                if (!$board->markValue($number)) {
                    $remainingBoards[] = $board;
                    continue;
                }

                if ($return) {
                    return $board->getSumUnChecked() * $number;
                }
            }
            $return = (count($remainingBoards) === 1);
            $this->boards = $remainingBoards;
        }
        return -1;
    }
}
