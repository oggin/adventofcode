<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles\_2021;

use AdventOfCode\Classes\MainRiddle;
use AdventOfCode\Riddles\_2021\Components\Day14\CharSequence;

class Day14 extends MainRiddle
{
    public static int $day = 14;
    public static int $year = 2021;
    private array $rules;
    private array $initalEmptyCount;
    private array $elementCounts;


    public function calcResult(): int
    {
        return $this->calcForCycles(10);
    }

    public function calcResult2(): int
    {
        return $this->calcForCycles(40);
    }

    private function calcForCycles(int $totalSteps): mixed
    {
        $rules = [];
        for ($idx = 2; $idx < count($this->lines); $idx++) {
            [$k, $v] = explode(" -> ", $this->lines[$idx]);
            $rules[$k] = $v;
        }

        $char = new CharSequence($this->lines[0], $rules);
        return $char->calcForCycles($totalSteps);



    }
}
