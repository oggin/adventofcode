<?php

namespace AdventOfCode\Riddles\_2021\Components\Day14;

class CharSequence
{
    private array $rules;
    private CharStack $initalEmptyCount;
    /**
     * @var array<int,array<string,CharStack>>
     */
    private array $cacheCharStackBySteps;
    private string $charSequence;

    public function __construct(string $charSequence, array $rules)
    {
        $this->charSequence = $charSequence;
        $this->rules = $rules;
    }

    private function doPair($charPair, $steps): CharStack
    {
        if ($steps === 0) {
            return new CharStack([]);
        }

        if (($stackFromCache = $this->getStackFromCache($steps, $charPair)) !== null) {
            return $stackFromCache;
        }

        $ruleReplacement = $this->rules[$charPair];
        $localStack = CharStack::fromStack($this->initalEmptyCount);
        $localStack->increaseForKey($ruleReplacement);

        $leftChar = $charPair[0];
        $rightChar = $charPair[1];
        foreach ($this->doPair($leftChar . $ruleReplacement, $steps - 1)->getStack() as $lChar => $lValue) {
            $localStack->increaseForKey($lChar, $lValue);
        }
        foreach ($this->doPair($ruleReplacement . $rightChar, $steps - 1)->getStack() as $rChar => $rValue) {
            $localStack->increaseForKey($rChar, $rValue);
        }
        $this->updateCache($steps, $charPair, $localStack);
        return $localStack;
    }

    public function calcForCycles(int $totalSteps): mixed
    {
        $this->cacheCharStackBySteps = array_fill(0, $totalSteps, []);
        $this->initalEmptyCount = new CharStack(array_fill_keys(array_unique(array_values($this->rules)), 0));
        $countByChar = CharStack::fromStack($this->initalEmptyCount);

        for ($i = 0; $i < (strlen($this->charSequence) - 1); $i++) {
            $charPair = substr($this->charSequence, $i, 2);
            $countByChar->increaseForKey($charPair[0]);
            foreach ($this->doPair($charPair, $totalSteps)->getStack() as $char => $count) {
                $countByChar->increaseForKey($char, $count);
            }
        }
        $countByChar->increaseForKey($this->charSequence[-1]);
        return $countByChar->getMaxCount() - $countByChar->getMinCount();
    }

    private function getStackFromCache($steps, $charPair): ?CharStack
    {
        if (array_key_exists($steps, $this->cacheCharStackBySteps) && array_key_exists($charPair, $this->cacheCharStackBySteps[$steps])) {
            return $this->cacheCharStackBySteps[$steps][$charPair];
        }
        return null;
    }

    private function updateCache(int $steps, string $charPair, CharStack $localStack): void
    {
        $this->cacheCharStackBySteps[$steps][$charPair] = $localStack;
    }
}
