<?php

namespace AdventOfCode\Riddles\_2021\Components\Day14;

class CharStack
{
    private array $stack;

    /**
     * @param array $stack
     */
    public function __construct(array $stack)
    {
        $this->stack = $stack;
    }

    public static function fromStack(CharStack $stack): self
    {
        return new self($stack->getStack());
    }

    public function increaseForKey(string $key, int $increase = 1): void
    {
        $this->stack[$key] += $increase;
    }

    /**
     * @return array<string,int>
     */
    public function getStack(): array
    {
        return $this->stack;
    }

    public function getMinCount()
    {
        asort($this->stack);
        return reset($this->stack);
    }

    public function getMaxCount()
    {
        asort($this->stack);
        return end($this->stack);
    }
}
