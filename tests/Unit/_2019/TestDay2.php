<?php

declare(strict_types=1);

namespace Unit\_2019;

use AdventOfCode\Riddles\_2019\Day2;
use AdventOfCode\Classes\MainTestCase;

class TestDay2 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day2(), 3306701);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day2(), 7621);
    }
}
