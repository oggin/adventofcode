<?php

declare(strict_types=1);

namespace Unit\_2019;

use AdventOfCode\Riddles\_2019\Day1;
use AdventOfCode\Classes\MainTestCase;

class TestDay1 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day1(), 3255932);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day1(), 4881041);
    }
}
