<?php

declare(strict_types=1);

namespace Unit\_2021;

use AdventOfCode\Riddles\_2021\Day8;
use AdventOfCode\Classes\MainTestCase;

class TestDay8 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day8(), 512);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day8(), 1091165);
    }
}
