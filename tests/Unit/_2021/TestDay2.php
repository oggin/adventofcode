<?php

declare(strict_types=1);

namespace Unit\_2021;

use AdventOfCode\Classes\MainTestCase;
use AdventOfCode\Riddles\_2021\Day2;

class TestDay2 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day2(), 1762050);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day2(), 1855892637);
    }
}
