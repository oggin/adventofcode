<?php

declare(strict_types=1);

namespace Unit\_2021;

use AdventOfCode\Riddles\_2021\Day9;
use AdventOfCode\Classes\MainTestCase;

class TestDay9 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day9(), 480);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day9(), 1045660);
    }
}
