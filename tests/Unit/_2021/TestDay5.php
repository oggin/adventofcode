<?php

declare(strict_types=1);

namespace Unit\_2021;

use AdventOfCode\Classes\MainTestCase;
use AdventOfCode\Riddles\_2021\Day5;

class TestDay5 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day5(), 6687);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day5(), 19851);
    }
}
