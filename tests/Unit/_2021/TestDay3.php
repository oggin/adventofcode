<?php

declare(strict_types=1);

namespace Unit\_2021;

use AdventOfCode\Riddles\_2021\Day3;
use AdventOfCode\Classes\MainTestCase;

class TestDay3 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day3(), 4138664);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day3(), 4273224);
    }
}
