<?php

declare(strict_types=1);

namespace Unit\_2021;

use AdventOfCode\Riddles\_2021\Day7;
use AdventOfCode\Classes\MainTestCase;

class TestDay7 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day7(), 341534);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day7(), 93397632);
    }
}
