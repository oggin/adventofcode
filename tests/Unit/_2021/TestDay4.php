<?php

declare(strict_types=1);

namespace Unit\_2021;

use AdventOfCode\Classes\MainTestCase;
use AdventOfCode\Riddles\_2021\Day4;

class TestDay4 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day4(), 54275);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day4(), 13158);
    }
}
