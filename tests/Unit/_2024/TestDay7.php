<?php

declare(strict_types=1);

namespace Unit\_2024;

use AdventOfCode\Riddles\_2024\Day7;
use AdventOfCode\Classes\MainTestCase;

class TestDay7 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day7(), 975671981569);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day7(), 223472064194845);
    }
}
