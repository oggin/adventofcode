<?php

declare(strict_types=1);

namespace Unit\_2024;

use AdventOfCode\Riddles\_2024\Day1;
use AdventOfCode\Classes\MainTestCase;

class TestDay1 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day1(), 1882714);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day1(), 19437052);
    }
}
