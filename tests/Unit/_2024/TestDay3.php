<?php

declare(strict_types=1);

namespace Unit\_2024;

use AdventOfCode\Riddles\_2024\Day3;
use AdventOfCode\Classes\MainTestCase;

class TestDay3 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day3(), 191183308);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day3(), 92082041);
    }
}
