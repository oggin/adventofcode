<?php

declare(strict_types=1);

namespace Unit\_2024;

use AdventOfCode\Riddles\_2024\Day4;
use AdventOfCode\Classes\MainTestCase;

class TestDay4 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day4(), 2644);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day4(), 1952);
    }
}
