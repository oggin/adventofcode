<?php

declare(strict_types=1);

namespace Unit\_2024;

use AdventOfCode\Riddles\_2024\Day5;
use AdventOfCode\Classes\MainTestCase;

class TestDay5 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day5(), 5713);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day5(), 5180);
    }
}
