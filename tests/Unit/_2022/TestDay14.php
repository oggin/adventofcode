<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day14;
use AdventOfCode\Classes\MainTestCase;

class TestDay14 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day14(), 592);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day14(), 30367);
    }
}
