<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day5;
use AdventOfCode\Classes\MainTestCase;

class TestDay5 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResultString(new Day5(), 'TLNGFGMFN');
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2String(new Day5(), 'FGLQJCMBD');
    }
}
