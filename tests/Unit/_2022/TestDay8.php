<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day8;
use AdventOfCode\Classes\MainTestCase;

class TestDay8 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day8(), 1782);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day8(), 474606);
    }
}
