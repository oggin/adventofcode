<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day15;
use AdventOfCode\Classes\MainTestCase;

class TestDay15 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day15(), 5181556);
    }

    public function testCalcResult2(): void
    {
        $this->markTestSkipped('not solved');
        //$this->assertCalcResult2(new Day15(), 15);
    }
}
