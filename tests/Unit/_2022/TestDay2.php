<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Classes\MainTestCase;
use AdventOfCode\Riddles\_2022\Day2;

class TestDay2 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day2(), 13809);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day2(), 12316);
    }
}
