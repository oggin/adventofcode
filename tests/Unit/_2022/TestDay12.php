<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Classes\MainTestCase;

class TestDay12 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->markTestSkipped('not solved');
       // $this->assertCalcResult(new Day12(), 12);
    }

    public function testCalcResult2(): void
    {
        $this->markTestSkipped('not solved');
        //$this->assertCalcResult2(new Day12(), 12);
    }
}
