<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Classes\MainTestCase;
use AdventOfCode\Riddles\_2022\Day3;

class TestDay3 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day3(), 7903);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day3(), 2548);
    }
}
