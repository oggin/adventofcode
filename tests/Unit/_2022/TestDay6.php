<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day6;
use AdventOfCode\Classes\MainTestCase;

class TestDay6 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day6(), 1912);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day6(), 2122);
    }
}
