<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day21;
use AdventOfCode\Classes\MainTestCase;

class TestDay21 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day21(), 21120928600114);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day21(), 3453748220116);
    }
}
