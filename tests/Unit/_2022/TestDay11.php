<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day11;
use AdventOfCode\Classes\MainTestCase;

class TestDay11 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day11(), 69918);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day11(), 19573408701);
    }
}
