<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day10;
use AdventOfCode\Classes\MainTestCase;

class TestDay10 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day10(), 14040);
    }

    public function testCalcResult2(): void
    {
        $expString = <<<EOT
####..##...##....##.####...##.####.#....
...#.#..#.#..#....#....#....#.#....#....
..#..#....#.......#...#.....#.###..#....
.#...#.##.#.......#..#......#.#....#....
#....#..#.#..#.#..#.#....#..#.#....#....
####..###..##...##..####..##..#....####.
EOT;
//ZGCJZJFL
        $this->assertCalcResult2String(new Day10(), $expString);
    }
}
