<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Classes\MainTestCase;
use AdventOfCode\Riddles\_2022\Day1;

class TestDay1 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day1(), 69206);
    }


    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day1(), 197400);
    }
}
