<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day13;
use AdventOfCode\Classes\MainTestCase;

class TestDay13 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day13(), 6395);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day13(), 24921);
    }
}
