<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day7;
use AdventOfCode\Classes\MainTestCase;

class TestDay7 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day7(), 1206825);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day7(), 9608311);
    }
}
