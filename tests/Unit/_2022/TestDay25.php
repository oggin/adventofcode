<?php

declare(strict_types=1);

namespace Unit\_2022;

use AdventOfCode\Riddles\_2022\Day25;
use AdventOfCode\Classes\MainTestCase;

class TestDay25 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResultString(new Day25(), '2-21=02=1-121-2-11-0');
    }
}
