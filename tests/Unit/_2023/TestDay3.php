<?php

declare(strict_types=1);

namespace Unit\_2023;

use AdventOfCode\Riddles\_2023\Day3;
use AdventOfCode\Classes\MainTestCase;

class TestDay3 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day3(), 531932);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day3(), 73646890);
    }
}
