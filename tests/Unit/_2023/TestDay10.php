<?php

declare(strict_types=1);

namespace Unit\_2023;

use AdventOfCode\Riddles\_2023\Day10;
use AdventOfCode\Classes\MainTestCase;

class TestDay10 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day10(), 10);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day10(), 10);
    }
}
