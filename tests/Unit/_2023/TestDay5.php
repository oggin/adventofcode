<?php

declare(strict_types=1);

namespace Unit\_2023;

use AdventOfCode\Riddles\_2023\Day5;
use AdventOfCode\Classes\MainTestCase;

class TestDay5 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day5(), 486613012);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day5(), 5);
    }
}
