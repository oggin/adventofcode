<?php

declare(strict_types=1);

namespace Unit\_2020;

use AdventOfCode\Riddles\_2020\Day8;
use AdventOfCode\Classes\MainTestCase;

class TestDay8 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day8(), 1915);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day8(), 944);
    }
}
