<?php

declare(strict_types=1);

namespace Unit\_2020;

use AdventOfCode\Riddles\_2020\Day13;
use AdventOfCode\Classes\MainTestCase;

class TestDay13 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day13(), 174);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day13(), 780601154795940);
    }
}
