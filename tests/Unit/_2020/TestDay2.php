<?php

declare(strict_types=1);

namespace Unit\_2020;

use AdventOfCode\Riddles\_2020\Day2;
use AdventOfCode\Classes\MainTestCase;

class TestDay2 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day2(), 414);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day2(), 413);
    }
}
