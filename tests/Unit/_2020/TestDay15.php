<?php

declare(strict_types=1);

namespace Unit\_2020;

use AdventOfCode\Riddles\_2020\Day15;
use AdventOfCode\Classes\MainTestCase;

class TestDay15 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day15(), 371);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day15(), 352);
    }
}
