<?php

declare(strict_types=1);

namespace Unit\_2020;

use AdventOfCode\Riddles\_2020\Day12;
use AdventOfCode\Classes\MainTestCase;

class TestDay12 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day12(), 796);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day12(), 39446);
    }
}
