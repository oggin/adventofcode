<?php

declare(strict_types=1);

namespace Unit\_2020;

use AdventOfCode\Riddles\_2020\Day1;
use AdventOfCode\Classes\MainTestCase;

class TestDay1 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day1(), 910539);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day1(), 116724144);
    }
}
