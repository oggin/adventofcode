<?php

declare(strict_types=1);

namespace Unit\_2020;

use AdventOfCode\Riddles\_2020\Day3;
use AdventOfCode\Classes\MainTestCase;

class TestDay3 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day3(), 225);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day3(), 1115775000);
    }
}
