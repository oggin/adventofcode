<?php

declare(strict_types=1);

namespace Unit\_2020;

use AdventOfCode\Riddles\_2020\Day9;
use AdventOfCode\Classes\MainTestCase;

class TestDay9 extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day9(), 400480901);
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day9(), 67587168);
    }
}
