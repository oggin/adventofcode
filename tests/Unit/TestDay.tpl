<?php

declare(strict_types=1);

namespace Unit\_{Y};

use AdventOfCode\Riddles\_{Y}\Day{X};
use AdventOfCode\Classes\MainTestCase;

class TestDay{X} extends MainTestCase
{
    public function testCalcResult(): void
    {
        $this->assertCalcResult(new Day{X}(), {X});
    }

    public function testCalcResult2(): void
    {
        $this->assertCalcResult2(new Day{X}(), {X});
    }
}
